<?php
// $id:$

/**
 * @file
 * This file contains the module's autoloader and registers it with
 * spl_autoload_register().
 */

/**
 * Autoloader to register with spl_autoload_register().
 *
 * @param string $class
 */
function _learnline_search_autoload($class) {
  $class_path = explode('\\', $class);

  if (implode('\\', array_slice($class_path, 0, 2)) === 'learnline\search') {

    $file = __DIR__ . '/' . implode('/', array_slice($class_path, 2)) .
        '.class.inc';

    if (is_file($file) && is_readable($file)) {
      include_once $file;
    } //if
  } //if
} //function

spl_autoload_register('_learnline_search_autoload');
