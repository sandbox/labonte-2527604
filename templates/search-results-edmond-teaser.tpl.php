<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

if (count($element['#results']) > 0): ?>
<div id="edmond_teaser" class="search-result clearfix">
  <div class="edmond_result" style="width: <?php print $element['#span_width'] ?>%;">
    <div>
      <div id="edmond_logo" class="clearfix">
        <?php print $element['#logos']; ?>
      </div>
      <a id="edmond_show_all" class="onpage" href="<?php print $element['#href']; ?>">
        <?php print t('Show all %count matches on Edmond NRW...', array('%count' => $element['#total'])); ?>
      </a>
    </div>
  </div>
  <?php $i = 1; foreach ($element['#results'] as $id => $result_obj): ?>
    <?php $result = $result_obj->getFormatted(); ?>
    <div class="edmond_result" style="width: <?php print $element['#span_width'] ?>%;">
      <div>
        <h4>
          <a href="<?php print $result['#href']; ?>" title="<?php print $result['#title']; ?>"
             target="_blank">
            <?php print $result['#title']; ?>
            <i class="fa fa-external-link"></i>
          </a>
        </h4>
        <?php if (isset($element['#type'])): ?>
          <span class="edmond_tease_type"><?php print $element['#type'] ?></span>
        <?php endif; ?>
      </div>
    </div>
    <?php if (++$i > $element['#max_display']) break; ?>
  <?php endforeach; ?>
</div>
<?php endif; ?>
