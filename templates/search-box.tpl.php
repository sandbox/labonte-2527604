<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

/**
 * Get template variables.
 */
global $base_url;
$front_page = drupal_is_front_page();
$favicon = theme_get_setting('favicon');
$logo = theme_get_setting('logo_path') ?: theme_get_setting('logo');
$site_name = variable_get('site_name', '');
$slogan = variable_get('site_slogan', '');

?>
<form id="search-box" accept-charset="UTF-8"
      class="<?php print $front_page ? 'front' : 'no-front'; ?>"
      action="/learnline/search" method="get">
  <?php if ($front_page): ?>
    <div class="row">
      <div class="col left" id="search-logo-container">
        <a href="/" title="<?php print $slogan; ?>">
          <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col left" id="search-input-container">
        <div><?php print render($element['search']); ?></div>
      </div>
      <div class="col right" id="search-button-container">
        <div id="search-button-wrapper">
          <button id="search-button" type="submit"><!-- #0272C1 -->
            <img alt="learnline-Suche" src="/sites/default/files/images/search-icon-white.png"/>
          </button>
        </div>
      </div>
    </div>
  <?php else: ?>
    <div class="row">
      <div class="col left" id="search-logo-container">
        <a href="/" title="<?php print $slogan; ?>">
          <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>
        </a>
      </div>
      <div class="subrow col left">
        <div class="col left" id="search-input-container">
          <div><?php print render($element['search']); ?></div>
        </div>
        <div class="col right" id="search-button-container">
          <div id="search-button-wrapper">
            <button id="search-button" type="submit"><!-- #0272C1 -->
              <img alt="learnline-Suche" src="/sites/default/files/images/search-icon-white.png"/>
            </button>
          </div>
          <?php if (!$is_front): ?>
            <div>
              <span id="advanced-search-toggle">
                <i class="fa fa-arrow-circle-down"></i>
                <?php print t('advanced search criteria'); ?> 
              </span>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
  <div class="row">
    <?php if ($is_front): ?>
      <div class="col right" id="advanced-search-toggle-container">
        <span id="advanced-search-toggle">
          <i class="fa fa-arrow-circle-down"></i>
          <?php print t('advanced search criteria'); ?> 
        </span>
      </div>
    <?php endif; ?>
  </div>
  <div class="row">
    <div class="col left" id="advanced-search-container">
      <?php print render($element['advanced_search']); ?>
    </div>
  </div>
  <div class="clearfix"></div>
</form>
