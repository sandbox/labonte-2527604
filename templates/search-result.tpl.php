<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

/**
 * $element = array(
 *  '#theme' => 'learnline_search_result',
 *  '#results' => array(
 *    0 => array(
 *      '#thumbnail' => array(
 *        '#theme' => 'learnline_search_result_thumbnail',
 *        '#images' => array(
 *          0 => [img src path],
 *          ...
 *        ),
 *      ),
 *    ),
 *    ...
 *  ),
 * );
 */

global $base_url, $search_result_build_count;

if (!isset($search_result_build_count)) {
  $search_result_build_count = 0;
}

$ajax_loader = $base_url . '/' . drupal_get_path('module', 'learnline_search') .
    '/img/ajax-loader.gif';
?>
<div class="search-result" data-url="<?php print $element['#origin']; ?>">
  <a class="search-result-anchor" name="<?php print $element['#id']; ?>"></a>
  <div class="search-result-teaser">
    <?php print render($element['#thumbnail']); ?>
    <div class="search-result-right-col">
      <h4 data-title="<?php print $element['#title']; ?>">
        <a href="<?php print $element['#href']; ?>" target="_blank" class="result-title">
          <?php print $element['#title']; ?>
          <i class="fa fa-external-link"></i>
        </a>
      </h4>
      <ul class="search-result-trigger clearfix">
        <li title="<?php print t('Toggle social share privacy bar.'); ?>"
            class="search-result-social-share-privacy">
          <?php print t('share'); ?>
          <i class="fa fa-facebook"></i>
          <i class="fa fa-twitter"></i>
          <i class="fa fa-google-plus"></i>
        </li>
        <li title="<?php print t('Toggle direct link box.'); ?>"
            class="search-result-direct-link">
          <?php print t('direct link'); ?>
          <i class="fa fa-link"></i>
        </li>
      </ul>
      <div class="search-result-rating">
        <img class="edutags-placeholder" style="display: none;"
             src="<?php print $ajax_loader; ?>"
             alt="<?php print t('Loading edutags ...'); ?>" />
      </div>
      <p>
        <span class="publisher" title="<?php print t('Publisher') . ': ' . implode(', ', $element['#publisher']); ?>">
          <?php print reset($element['#publisher']); ?>
          <?php if (!empty($element['#publisher'])) { print '<br/>'; } ?>
        </span>
        <?php // Prepare #contenttype and #mediatype
          $types = array_merge($element['#resourcetype_backup'], array($element['#contenttype']), array($element['#contenttype_backup']));
        ?>
        <span title="<?php print t('Media type'); ?>"
              data-media-type="<?php print base64_encode(json_encode($types)); ?>">
          <?php print $element['#resourcetype']; ?>
          <?php if (!empty($element['#contenttype'])) print ' <em>(' . $element['#contenttype'] . ')</em>'; ?>
        </span>
        <?php if ((!empty($element['#contenttype']) || !empty($element['#resourcetype'])) && !empty($element['#filesize'])): ?>
          <span title="<?php print t('File size'); ?>">
            <?php print ' | ' . $element['#filesize']; ?>
          </span>
        <?php endif; ?>
      </p>
      <?php if(isset($element['#cc']) && !empty($element['#cc'])): ?>
        <img src="<?php print $element['#cc']['#src']; ?>"
             alt="<?php print $element['#cc']['#alt']; ?>"
             class="search-result-cc"/>
      <?php endif; ?>
    </div>
    <div class="search-result-more-toggle">
      <?php print t('more'); ?>
      <i class="fa fa-arrow-down"></i>
    </div>
    <span class="clearfix"></span>
  </div>
  <div class="search-result-more">
    <?php if (!empty($element['#description'])): ?>
      <div>
        <strong class="search-result-left-col"><?php print t('Description'); ?>:</strong>
        <p class="search-result-right-col search-result-description">
          <?php print $element['#description']; ?>
        </p>
      </div>
    <?php endif; ?>
    <div>
      <strong class="search-result-left-col"><?php print t('School subject'); ?>:</strong>
      <?php if (!empty($element['#subject']) && is_array($element['#subject'])): ?>
        <p class="search-result-right-col search-result-subject"
           data-subject="<?php print base64_encode(json_encode($element['#subject'])); ?>">
          <?php print implode(', ', $element['#subject']); ?>
        </p>
      <?php else: ?>
        <p class="search-result-right-col search-result-subject">
          <?php print t('Not specified.'); ?>
        </p>
      <?php endif; ?>
    </div>
    <div>
      <strong class="search-result-left-col"><?php print t('Typical educational age'); ?>:</strong>
      <p class="search-result-right-col">
        <?php print !empty($element['#age'])
            ? implode(', ', $element['#age'])
            : t('Not specified.'); ?>
      </p>
    </div>
    <?php if (!empty($element['#usage'])): ?>
      <div>
        <strong class="search-result-left-col"><?php print t('Usage notice'); ?>:</strong>
        <p class="search-result-right-col">
          <?php print $element['#usage']; ?>
        </p>
      </div>
    <?php endif; ?>
  </div>
  <div class="search-result-footer">
    <div class="search-result-edutags-box" style="display: none;"></div>
    <div class="search-result-social-share-privacy-box clearfix">
      <?php
        /**
         * We'll use socialshareprivacy module's internal (underscore prefixed)
         * functions to add the social share privacy bar to each search result.
         * Not the nicest way, but working...
         */
        $ssp_url = $base_url . $element['#href'];
        $ssp_node = md5($ssp_url);
        $ssp_markup = _socialshareprivacy_get_markup($ssp_node);

        print render($ssp_markup);
        _socialshareprivacy_write_javascript($ssp_node, $ssp_url);
      ?>
    </div>
    <div class="search-result-direct-link-box clearfix">
      <input type="text" readonly="readonly"
             value="<?php print $base_url . $element['#href'] ?>"/>
    </div>
  </div>
</div>
