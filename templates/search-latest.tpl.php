<?php
// $id:$

/**
 * @file
 * Template file for displaying the latest learn:line NRW resources.
 */
?>
<h2>
  <span class="headline"><?php print $element['#block']->subject; ?></span>
</h2>
<div id="latest-resources">
  <?php 
  /*** PHP CODE BLOCK ***/
  if (isset($element['#children']) && count($element['#children']) > 0) {
    $result = reset($element['#children']);
    // As long as there is no colon syntax for a do-while loop, we need to use 
    // classical curly braces for this...
    do {
      if (isset($result['#description']) && is_string($result['#description'])) {
        if (($l = strlen($result['#description'])) > 75) {
          $teaser_end = strpos($result['#description'], ' ', 75);
          $teaser_txt = substr($result['#description'], 0, $teaser_end) . '...';
        } //if
        elseif ($l > 0) {
          $teaser_txt = $result['#description'];
        } //else
      } //if
  /*** PHP CODE BLOCK ***/
  ?>
    <a href="<?php print $result['#singlesearch']; ?>">
      <div class="search-result-latest">
        <h3><?php print $result['#title']; ?></h3>
        <?php if (is_string($result['#thumbnail']['#src'])): ?>
          <figure style="margin: 0 10px 10px 0 !important;">
            <img src="<?php print $result['#thumbnail']['#src']; ?>"/>
          </figure>
        <?php endif; ?>
        <?php if (!isset($teaser_txt)): ?>
          <p>
            <span title="<?php print t('Media type'); ?>">
              <?php print $result['#resourcetype']; ?>
              <?php !empty($result['#contenttype']) && print ' <em>(' . $result['#contenttype'] . ')</em>'; ?>
            </span>
            <?php if ((!empty($result['#contenttype']) || !empty($result['#resourcetype'])) && !empty($result['#filesize'])): ?>
              <span title="<?php print t('File size'); ?>">
                <?php print ' | ' . $result['#filesize']; ?>
              </span>
            <?php endif; ?>
          </p>
          <p><em><?php print t('No description found.'); ?></em></p>
        <?php else: ?>
          <p><?php print $teaser_txt; ?></p>
        <?php endif; ?>
        <div class="clearfix"></div>
      </div>
    </a>
  <?php 
  /*** PHP CODE BLOCK ***/
    } while ($result = next($element['#children']));
  } //if
  /*** PHP CODE BLOCK ***/
  ?>
</div>
