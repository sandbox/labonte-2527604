<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */
?>
<tr>
  <td>
    <a title="<?php print $element['#title']; ?>"
       href="<?php print $element['#href']; ?>"
       target="_blank">
      <?php print $element['#title']; ?>
      <i class="fa fa-external-link"></i>
    </a>
  </td>
  <td>
    <?php if (isset($element['#type']) && is_string($element['#type'])): ?>
      <?php print $element['#type']; ?> 
    <?php endif; ?>
  </td>    
  <td>
    <?php if (isset($element['#year']) && is_string($element['#year'])): ?>
      <?php print $element['#year']; ?> 
    <?php endif; ?>
  </td>
  <td>
    <?php if(isset($element['#publisher']) && is_string($element['#publisher'])): ?>
      <?php print $element['#publisher']; ?> 
    <?php endif; ?>
  </td>
</tr>
<?php if (isset($element['#text']) && is_string($element['#text'])): ?>
<tr style="display: none;">
  <td colspan="4">
    <?php print $element['#text']; ?> 
  </td>
</tr>
<?php endif; ?>
