<?php
// $id:$

/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 */

/**
 * $element = array(
 *  '#theme' => 'learnline_search_result',
 *  '#children' => array(
 *    0 => array(
 *      '#thumbnail' => array(
 *        '#theme' => 'learnline_search_result_thumbnail',
 *        '#images' => array(
 *          0 => [img src path],
 *          ...
 *        ),
 *      ),
 *    ),
 *    ...
 *  ),
 * );
 */

global $base_url;

$from = ($element['#page_no'] -1) * $element['#per_page'];
$to = $from + $element['#per_page'];

?>
<?php if (!empty($element['#children'])): ?>
  <div class="search-result-header clearfix">
    <span class="search-result-from-to">
      <?php print t('Showing results') . ' ' . ($from +1) . '&nbsp;-&nbsp;' .
          ($to > $element['#total'] ? $element['#total'] : $to) .
          ' ' . t('of') . ' ' . $element['#total']; ?>
    </span>
  </div>
<?php endif; ?>
<?php
  if (isset($element['#error'])) {
    print '<p>' . implode('</p><p>', $element['#error']) . '</p>';
  } //if
  elseif (count($element['#children']) < 1) {
    $no_results_text = variable_get(LearnlineSearch::SETTING__NO_RESULTS_FOUND);
    print str_replace('%searchword', $element['#searchword'],
        $no_results_text['value']);
  } //else
?>
<?php $i = 0; foreach ($element['#children'] as $id => $result): ?>
  <?php print $result->getOutput(); ?>
  <?php if ((++$i === 2 || count($element['#children']) === $i+1) && !isset($edmond_printed)) {
    print render($element['#edmond_teaser']);
    $edmond_printed = TRUE;
  } ?>
<?php endforeach; ?>
<?php if (!isset($edmond_printed)): ?>
  <?php print render($element['#edmond_teaser']); ?>
<?php endif; ?>
<div class="search-result-pagination">
  <?php
    // Determine last page number and thereby the total page count:
    $page_last = ceil($element['#total'] / $element['#per_page']);
    // Determine previous page number:
    if (($page_prev = $element['#page_no'] -1) < 1) {
      $page_prev = 1;
    }
    // Determine next page number:
    if (($page_next = $element['#page_no'] +1) > $page_last) {
      $page_next = $page_last;
    }
    // Determine direct page links:
    $page_direct_links = array();
    // Direct page linking for pages lower than the current one:
    $i = 0;
    $no = $element['#page_no'];
    while ($no > 1) {
      if (++$i > 3) {
        $page_direct_links[] = ' ... ';
        $i--;
        break;
      } //if
      $no--;
    } //while

    while ($no < $element['#page_no']) {
      $page_direct_links[] = '<a href="' . $element['#pagination_base'] . $no .
          '"> ' . $no . ' </a>';
      $no++;
    }
    // Direct page links for pages higher than the current one:
//    $i = 0;
    $no = $element['#page_no'];
    $page_direct_links[] = "<a href='' style='font-size:1.2em;'> {$element['#page_no']} </a>";
    while ($no < $page_last) {
      if (++$i > 6) {
        $page_direct_links[] = ' ... ';
        break;
      } //if
      $page_direct_links[] = '<a href="' . $element['#pagination_base'] . ++$no .
          '"> ' . $no . ' </a>';
    } //while
  ?>
  <?php if ($page_last > 1): ?>
    <a title="<?php print t('go to first page'); ?>"
       href="<?php print $element['#pagination_base'] . '1'; ?>">
      &laquo;&nbsp;<?php print t('first'); ?>
    </a>|<a
       title="<?php print t('go to previous page'); ?>"
       href="<?php print $element['#pagination_base'] . $page_prev; ?>">
      &lsaquo;&nbsp;<?php print t('previous'); ?>
    </a>|<?php print implode('|', $page_direct_links); ?>|<a
       title="<?php print t('go to next page'); ?>"
       href="<?php print $element['#pagination_base'] . $page_next; ?>">
      <?php print t('next'); ?>&nbsp;&rsaquo;
    </a>|<a
       title="<?php print t('go to last page'); ?>"
       href="<?php print $element['#pagination_base'] . $page_last; ?>">
      <?php print t('last'); ?>&nbsp;&raquo;
    </a>
  <?php endif; ?>
</div>
<div>
  <pre>
    <?php echo htmlentities($element['#bottom']); ?>
  </pre>
</div>
<div id="edutags-fake" style="display: none;">
  <?php 
    $edutags_dummy = array(
      '#id' => 0,
      '#tags' => array(),
      '#comments' => array(),
      '#rating' => 0,
      '#rating_class' => '',
      '#theme' => 'learnline_search_result_edutags',
    );
    
    print render($edutags_dummy);
  ?>
</div>
