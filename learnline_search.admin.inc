<?php
// $Id:$

/**
 * @file
 * Module's admin settings.
 */

use learnline\search\classes\sodis\Service as SodisService;
use learnline\search\classes\edmond\Service as EdmondService;
use learnline\search\classes\edutags\Service as EdutagsService;

/**
 * Callback function for general administration settings.
 *
 * @return array Renderable form.
 */
function learnline_search_admin_settings_general() {
  $settings = array();

  $default_value = variable_get(LearnlineSearch::SETTING__ADVANCED_SEARCH_OPTIONS);
  $settings[LearnlineSearch::SETTING__ADVANCED_SEARCH_OPTIONS] = array(
//    '#type' => 'textarea',
    '#type' => 'text_format',
    '#format' => $default_value['format'],
    '#title' => t('Advanced search'),
    '#description' => t("Defines the advanced search options either as plain HTML or as an array following the convetions of Drupal's Form API."),
    '#default_value' => $default_value['value'],
    '#weight' => 10,
  );

  $settings[LearnlineSearch::SETTING__LEGAL_NOTICE_HEADING] = array(
    '#type' => 'textfield',
    '#title' => t('Legal notice title'),
    '#description' => t("Shown as heading for the legal notice."),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => variable_get(LearnlineSearch::SETTING__LEGAL_NOTICE_HEADING),
    '#weight' => 19,
  );

  $default_value = variable_get(LearnlineSearch::SETTING__LEGAL_NOTICE);
  $settings[LearnlineSearch::SETTING__LEGAL_NOTICE] = array(
//    '#type' => 'textarea',
    '#type' => 'text_format',
    '#format' => $default_value['format'],
    '#rows' => 25,
    '#title' => t('Legal notice'),
    '#description' => t('Define the legal notice for search results, which is shown on top of the right sidebar.'),
    '#default_value' => $default_value['value'],
    '#weight' => 20,
  );

  $default_value = variable_get(LearnlineSearch::SETTING__NO_RESULTS_FOUND);
  $settings[LearnlineSearch::SETTING__NO_RESULTS_FOUND] = array(
//    '#type' => 'textarea',
    '#type' => 'text_format',
    '#format' => $default_value['format'],
    '#rows' => 25,
    '#title' => t('Message if no results were found'),
    '#description' => t('Define what to display if the current search request does not deliver any result.<br/> You can use %searchword as variable for the currently searched term.'),
    '#default_value' => $default_value['value'],
    '#weight' => 25,
  );

  $settings[LearnlineSearch::SETTING__RESULTS_PER_PAGE] = array(
    '#type' => 'textfield',
    '#title' => t('Results per page'),
    '#description' => t("Used for pagination."),
    '#size' => 2,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#default_value' => variable_get(LearnlineSearch::SETTING__RESULTS_PER_PAGE),
    '#rules' => array('numeric', 'length[1,3]'),
    '#weight' => 30,
  );

  $settings[SodisService::SETTING__TYPEAHEAD_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('Type-ahead result count'),
    '#description' => t("Defines how many suggestions are shown to the user."),
    '#size' => 2,
    '#maxlength' => 3,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__TYPEAHEAD_COUNT),
    '#rules' => array('numeric', 'length[1,3]'),
    '#weight' => 40,
  );

  $settings[LearnlineSearch::SETTING__PERSISTENT_SETTINGS] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep settings persistent'),
    '#description' => t('Keep the module\'s settings over deinstallation.'),
    '#default_value' => variable_get(LearnlineSearch::SETTING__PERSISTENT_SETTINGS),
    '#weight' => 50,
  );

  $settings[LearnlineSearch::SETTING__CC_DIR] = array(
    '#type' => 'textfield',
    '#title' => t('Copyright logos directory'),
    '#description' => t("Path to the directory where the CC images can be found (absolute or relative to Drupal's root directory).)"),
    '#default_value' => variable_get(LearnlineSearch::SETTING__CC_DIR),
    '#weight' => 60,
  );

  return system_settings_form($settings);
} //function

/**
 * Callback function for SODIS specific administration settings.
 *
 * @return array Renderable form.
 */
function learnline_search_admin_settings_sodis($form_id, $form_state) {
  $settings = array();

  $settings[SodisService::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate SODIS search service'),
    '#description' => '<strong>' .
        t('Deactivation will also disable the type-ahead functionality.') .
        '</strong>',
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[SodisService::SETTING__SEARCH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Search service address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/search.json<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__SEARCH_URI),
    '#weight' => 20,
  );

  $settings[SodisService::SETTING__TYPEAHEAD_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Type-ahead service address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/type-ahead.json<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__TYPEAHEAD_URI),
    '#weight' => 30,
  );

  $settings[SodisService::SETTING__RESOURCE_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Resource address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/objects/%RESOURCE<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%RESOURCE</code> ' . t('will be replaced by the appropriate resource ID.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__RESOURCE_URI),
    '#weight' => 40,
  );

  $settings[SodisService::SETTING__RESOURCE_DOWNLOAD_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Resource download address'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/objects/%RESOURCE/download<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%RESOURCE</code> ' . t('will be replaced by the appropriate resource ID.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__RESOURCE_DOWNLOAD_URI),
    '#weight' => 50,
  );

  $settings[SodisService::SETTING__FACETS_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Base URI for requesting lists of facet values'),
    '#description' => 'Syntax: http://sodis.example.com/learnline3-rest/rest/' .
        '<code>%KEY</code>/facets/%FACET<br/>' .
        '(<code>%KEY</code> ' . t('will be replaced by the API-Key.') . ')<br/>' .
        '(<code>%FACET</code> ' . t('will be replaced by the appropriate facet.') . ')',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__FACETS_URI),
    '#weight' => 60,
  );

  $settings[SodisService::SETTING__ACCESS_TOKEN] = array(
    '#type' => 'textfield',
    '#title' => t('API-Key'),
    '#description' => t('Please enter a valid access token.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__ACCESS_TOKEN),
    '#weight' => 70,
  );

  $settings[SodisService::SETTING__THUMBS_DIR] = array(
    '#type' => 'textfield',
    '#title' => t('Thumbs folder'),
    '#description' => t('Path to default thumbnail images directory.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__THUMBS_DIR),
    '#weight' => 80,
  );
  
  $settings[SodisService::SETTING__GET_LATEST_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('Number of resources to display as latest material'),
    '#description' => t("Define how many resource links will be listed as latest on front page."),
    '#size' => 2,
    '#maxlength' => 3,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__GET_LATEST_COUNT),
    '#rules' => array('numeric', 'length[1,3]'),
    '#weight' => 90,
  );
  
  $settings[SodisService::SETTING__GET_LATEST_SORTING] = array(
    '#type' => 'select',
    '#title' => t('Sorting criteria for latest resources'),
    '#description' => t('Key to sort resources by.'),
    '#required' => FALSE,
    '#options' => SodisService::getSortings(),
    '#empty_option' => t('standard (relevance)'),
    '#empty_value' => '',
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__GET_LATEST_SORTING),
    '#weight' => 95,
  );

  $settings[SodisService::SETTING__GET_LATEST_RESTRICTION] = array(
    '#type' => 'textfield',
    '#title' => t('Restriction for getting latest resources'),
    '#description' => t('This is necessary to work around the fact, that the API encounters an error on requests without .'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => SodisService::getSetting(
        SodisService::SETTING__GET_LATEST_RESTRICTION),
    '#weight' => 99,
  );

  return system_settings_form($settings);
} //function

/**
 * Callback function for EDMOND specific administration settings.
 *
 * @return array Renderable form.
 */
function learnline_search_admin_settings_edmond() {
  $settings = array();

  $settings[EdmondService::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate EDMOND search service'),
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[EdmondService::SETTING__SEARCH_URI] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond/Arix service address'),
    '#description' => 'Syntax: http://edmond.example.com/arix/',
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__SEARCH_URI),
    '#weight' => 20,
  );

  $settings[EdmondService::SETTING__ROOT_IDENTITY] = array(
    '#type' => 'textfield',
    '#title' => t('Edmond/Arix root identity'),
    '#description' => t("Default value: 'NRW'."),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__ROOT_IDENTITY),
    '#weight' => 30,
  );

  $settings[EdmondService::SETTING__PASSWORD] = array(
    '#type' => 'textfield',
    '#title' => t('Authentication secret'),
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__PASSWORD),
    '#weight' => 40,
  );

  $settings[EdmondService::SETTING__MAX_RESULTS] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum result count'),
    '#description' => t('Maximum number of results to be displayed. <br/> Be aware of the visual appearance, when you change this setting.'),
    '#size' => 3,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__MAX_RESULTS),
    '#weight' => 50,
  );

  $settings[EdmondService::SETTING__LOGO] = array(
    '#type' => 'textarea',
    '#title' => t('Edmond logo'),
    '#description' => t('Here you can alter the HTML snippet retrieved per API to display as logo.'),
    '#rows' => 4,
    '#required' => FALSE,
    '#default_value' => EdmondService::getSetting(
        EdmondService::SETTING__LOGO),
    '#prefix' => '<div id="textarea-edmond-logo" class="clearfix"><div>',
    '#suffix' => '</div>' .
        '<div><label>' . t('Logo preview') . '</label><div>' .
        EdmondService::getSetting(EdmondService::SETTING__LOGO) .
        '</div></div></div>',
    '#weight' => 60,
  );

  $settings['fetch_logo'] = array(
    '#type' => 'submit',
    '#title' => t('Fetch logo(s) from Edmond/Arix service'),
    '#description' => t('Fetches/Updates logo(s) from Edmond/Arix service.'),
    '#submit' => array('learnline_search_admin_edmond_fetch_logo'),
    '#value' => t('Fetch logo via API'),
    '#weight' => 70,
    '#parent' => $settings[EdmondService::SETTING__LOGO],
  );

  $settings['#submit'] = array('learnline_search_admin_settings_edmond_submit');

  return system_settings_form($settings);
} //function

function learnline_search_admin_settings_edutags() {
  $settings = array();

  $settings[EdutagsService::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate Edutags'),
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[EdutagsService::SETTING__DOMAIN] = array(
    '#type' => 'textfield',
    '#title' => t('Edutags domain'),
    '#description' => t("eg. 'www.edutags.de' or 'www.edutags-dev.de'."),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__DOMAIN),
    '#weight' => 20,
  );

  $settings[EdutagsService::SETTING__RESOURCE_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Edutags-API path'),
    '#description' => 'Syntax: http://www.edutags.de/api/v1/%KEY/%ID/get_resource<br/>' .
        t('%KEY will be replaced by the appropriate access token.<br/>') .
        t('%ID will be replaced by the appropriate resource ID.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__RESOURCE_PATH),
    '#weight' => 22,
  );

  $settings[EdutagsService::SETTING__ACCESS_TOKEN] = array(
    '#type' => 'textfield',
    '#title' => t('Access token (User-ID)'),
    '#description' => t('This is the user ID provided by the Edutags administrator.'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__ACCESS_TOKEN),
    '#weight' => 24,
  );

  $settings[EdutagsService::SETTING__BOOKMARKLET_PATH] = array(
    '#type' => 'textfield',
    '#title' => t('Bookmarklet path'),
    '#description' => t("Path (beginning with a slash '/') to where to stage the bookmarklet requests."),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__BOOKMARKLET_PATH),
    '#weight' => 26,
  );

  $settings[EdutagsService::SETTING__MAX_TAG_COUNT] = array(
    '#type' => 'textfield',
    '#title' => t('Tag limit'),
    '#description' => t('Maximum number of tags to list per result.'),
    '#size' => 2,
    '#maxlength' => 5,
    '#required' => TRUE,
    '#default_value' => EdutagsService::getSetting(
        EdutagsService::SETTING__MAX_TAG_COUNT),
    '#rules' => array('numeric', 'length[1,5]'),
    '#weight' => 40,
  );
  
  $settings['#submit'] = array('learnline_search_admin_settings_edutags_submit');
  
  return system_settings_form($settings);
} //function

function learnline_search_admin_edmond_fetch_logo($form, &$form_state) {
  EdmondService::getInstance()->fetchLogo();
} //function

function learnline_search_admin_settings_general_submit($form, &$form_state) {
  learnline_search_admin_settings_submit($form, $form_state);
} //function

function learnline_search_admin_settings_sodis_submit($form, &$form_state) {
  learnline_search_admin_settings_submit($form, $form_state);
} //function

function learnline_search_admin_settings_edmond_submit($form, &$form_state) {
  if ($form_state['values'][EdmondService::SETTING__LOGO]
      !== variable_get(EdmondService::SETTING__LOGO)) {
    $form_state['values'][EdmondService::SETTING__LOGO] = strip_tags(
        $form_state['values'][EdmondService::SETTING__LOGO], '<img><a>');
  } //if
  learnline_search_admin_settings_submit($form, $form_state);
} //function

function learnline_search_admin_settings_edutags_submit($form, &$form_state) {
  learnline_search_admin_settings_submit($form, $form_state);
} //function

/**
 * Implements a custom submit handler.
 */
function learnline_search_admin_settings_submit($form, &$form_state) {
  if (isset($form_state['values']) && !empty($form_state['values'])) {
    cache_clear_all();
    drupal_set_message('Cleared all caches.');
  } //if
} //function
