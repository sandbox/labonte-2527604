<?php
// $id:$

/**
 * @file
 * This file contains the SearchResult class interface.
 */

namespace learnline\search\interfaces;

interface SearchResult extends View {
  /**
   * Returns the external URL of the requested resource.
   *
   * @return  string                The external resource URL.
   */
  public function getResourceLocation();
  
  /**
   * Returns the service identifier.
   * 
   * @return string
   */
  public function getServiceId();
} //interface
