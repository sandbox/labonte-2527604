<?php
// $id:$

/**
 * @file
 * This file contains the SearchRequest class interface.
 */

namespace learnline\search\interfaces;

use Countable;

interface SearchResponse extends View, Countable {
  /**
   * Returns an array containing all results.
   * Array keys are the appropriate result IDs.
   *
   * @return \SearchResult[] The actual results.
   */
  public function getResults();

  /**
   * Returns the result specified by its index.
   *
   * @param  string $index Result ID.
   * @return \SearchResult The search result with ID $index.
   */
  public function getResult($index);

  /**
   * Get the originating HTTP request as raw string.
   *
   * @return string
   */
  public function getRequestString();

  /**
   * Get HTTP response code.
   *
   * @return int
   */
  public function getStatusCode();

  /**
   * Get the HTTP status message.
   *
   * @return string
   */
  public function getStatusMessage();

  /**
   * Get HTTP error message or FALSE if no error occurred.
   *
   * @return string|FALSE
   */
  public function getError();

  /**
   * Get the HTTP version string e.g. 'HTTP/1.1'.
   *
   * @return string|FALSE
   */
  public function getVersion();

  /**
   * Get the initial HTTP status code, if the request was redirected.
   *
   * @return int|FALSE
   */
  public function getRedirectStatusCode();

  /**
   * Get the target's URL string in case of a redirect; otherwise FALSE.
   *
   * @return string|FALSE
   */
  public function getRedirectUrl();

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   */
  public function getHeaders();

  /**
   * Returns the raw response body.
   *
   * @return string|FALSE
   */
  public function getRawData();

  /**
   * Returns an appropriate PHP representation for response data.
   *
   * Data format depends on the service and should be in an appropriate PHP
   * representation for the raw response format.
   * (eg. JSON -> Array/ArrayObject, XML -> SimpleXMLElement)
   *
   * @return mixed
   */
  public function getData();
  
  /**
   * Returns last received/built instance of Response class.
   * 
   * @return \learnline\search\interfaces\SearchResponse
   */
  public static function getLast();
} //interface
