<?php
// $id:$

/**
 * @file
 * This file contains the Request class interface.
 */

namespace learnline\search\interfaces;

interface SearchRequest {
  /**
   * Request method: GET.
   *
   * @var string
   */
  const METHOD__GET = 'GET';

  /**
   * Request method: POST.
   *
   * @var string
   */
  const METHOD__POST = 'POST';

  /**
   * Default value: \Request->setMaxRedirects().
   *
   * @var int
   */
  const DEFAULT__MAX_REDIRECTS = 3;

  /**
   * Default value: \Request->setTimeout().
   *
   * @var float
   */
  const DEFAULT__EXEC_TIMEOUT = 30.0;

  /**
   * Returns a new Request.
   *
   * @return \Request Returns itself.
   */
  public static function create();

  /**
   * Unsets all values of the current request.
   *
   * @return \Request Returns itself.
   */
  public function reset();

  /**
   * Returns the stream context resource if set, otherwise FALSE.
   *
   * @return resource|FALSE
   */
  public function getStreamContext();

  /**
   * Sets a resource context.
   *
   * A resource context can be produced using PHP's native function
   * stream_context_create().
   *
   * @param  resource $context A stream context resource.
   * @return \Request    Returns itself.
   */
  public function setStreamContext($context);

  /**
   * Returns the URL to which the request will be staged.
   *
   * @return string|FALSE
   */
  public function getUrl();

  /**
   * Set the request target URL.
   *
   * @param  string $url   URL to which to stage the request.
   * @return \SearchResult Returns itself.
   */
  public function setUrl($url);

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   */
  public function getHeaders();

  /**
   * Set the HTTP headers as associative array.
   *
   * @param  array $headers Associative array containing the request headers.
   * @return \Request Returns itself.
   */
  public function setHeaders($headers);

  /**
   * Returns the request method.
   *
   * @return string
   */
  public function getMethod();

  /**
   * Sets the request method to use.
   *
   * @param  string $method Either \Request::GET or \Request::POST.
   * @return \Request Returns itself.
   */
  public function setMethod($method = 'GET');

  /**
   * Returns the maximum execution time of the request.
   *
   * @return float
   */
  public function getTimeout();

  /**
   * Set the request timeout.
   *
   * This timeout takes effect no matter if a response is received or not: When
   * the timeout exceeds, the request is canceled.
   *
   * @param  float $seconds Timeout.
   * @return \Request
   */
  public function setTimeout($seconds = 30.0);

  /**
   * Returns the maximum number of redirects accepted.
   *
   * @return int
   */
  public function getMaxRedirects();

  /**
   * Defines the maximum acceptable count of redirects.
   *
   * @param  int $count     Number of accepted redirects.
   * @return \Request Returns itself.
   */
  public function setMaxRedirects($count = 3);

  /**
   * Returns the resquest data/body.
   *
   * @return array|FALSE
   */
  public function getData();

  /**
   * Takes an associative array, puts it into URL-encoded format and sets the
   * result as request body.
   *
   * @return \Request Returns itself.
   */
  public function setData($data);

  /**
   * Returns the raw request body.
   *
   * @return string|FALSE
   */
  public function getRawData();

  /**
   * Takes data as it is for the request body.
   *
   * @return \Request
   */
  public function setRawData($data);

  /**
   * Get the corresponding \SearchResponse.
   *
   * @return \SearchResponse|FALSE
   */
  public function getResponse();

  /**
   * Returns an array containing all results.
   * Array keys are the appropriate result IDs.
   *
   * @return \SearchResult[]|FALSE The actual results.
   */
  public function getResults();

  /**
   * Returns the result specified by its index.
   *
   * @param  string $index Result ID.
   * @return \learnline\search\interfaces\SearchResult|FALSE The search result with ID $index.
   */
  public function getResult($index);

  /**
   * Send HTTP request and get response.
   *
   * @return \learnline\search\interfaces\http\Request Returns itself.
   */
  public function send();
} //interface
