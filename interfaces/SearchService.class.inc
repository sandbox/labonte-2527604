<?php
// $id:$

/**
 * @file
 * This file contains the SearchService class interface.
 */

namespace learnline\search\interfaces;

interface SearchService {
  /**
   * Cache table to be used by a specific service implementation.
   *
   * @var string
   */
  const CACHE = 'cache';

  /**
   * Variable value: Required.
   *
   * Use this if you cannot determine a setting's default value. Apparently it
   * is not possible to define NULL as a default value, without specifying this
   * value as required! This is caused by the behavior of Drupal's
   * variable_get() function.
   *
   * @var NULL
   */
  const REQUIRED = NULL;

  /**
   * This method should simply return an associative array with default
   * values. Array keys should be the corresponding setting names.
   * Using a method instead of a simple variable for this approach gives us the
   * flexibility to determine dynamic default values.
   *
   * Use SearchService::REQUIRED or simply NULL for mandatory settings.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   */
  public static function getDefaultSettings();

  /**
   * Returns all settings as associative array.
   *
   * @return array
   */
  public static function getSettings();

  /**
   * Saves multiple settings at once.
   *
   * @param array $settings Associative array containing multiple settings.
   * @return \learnline\search\interfaces\SearchService Returns itself.
   * @throws \RuntimeException
   */
  public static function setSettings($settings);

  /**
   * Returns the specified setting's value.
   *
   * @return mixed
   * @throws \RuntimeException
   */
  public static function getSetting($setting);

  /**
   * Save a setting.
   *
   * @param  string $setting Setting name.
   * @param  mixed $value Value to set.
   * @return \learnline\search\interfaces\SearchService Returns itself.
   * @throws \RuntimeException
   */
  public static function setSetting($setting, $value);

  /**
   * Load setting from database.
   *
   * This method doesn't care if the variable has already been loaded or about
   * its sync status.
   *
   * @param  string $setting
   * @return mixed
   */
  public static function loadSetting($setting);

  /**
   * Store current settings to database.
   *
   * @return \learnline\search\classes\SearchService Returns itself.
   */
  public static function save();
} //interface
