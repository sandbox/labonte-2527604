<?php
// $id:$

/**
 * @file
 * This file contains the SearchResult class interface.
 */

namespace learnline\search\interfaces;

use IteratorAggregate;
use ArrayAccess;

interface View extends IteratorAggregate, ArrayAccess {
  /**
   * Parameter: Render format.
   *
   * @var string
   */
  const FORMAT = 'format';

  /**
   * Parameter value: Render format HTML.
   *
   * @var string
   */
  const FORMAT__HTML = 'html';

  /**
   * Returns a new SearchResult.
   *
   * In this context 'raw' means some native format, that should be handled by
   * this view to get a renderable array.
   *
   * @uses $this->parse()
   *
   * @param  mixed $raw    Raw input data.
   * @return \learnline\search\interfaces\View Returns new instance of View.
   */
  public static function create($raw);

  /**
   * Parse raw result data.
   *
   * @param  mixed $raw    Raw input data.
   * @return \learnline\search\interfaces\View Returns itself.
   */
  public function parse($raw);

  /**
   * Set parameters.
   * Overrides existing values.
   *
   * @param  array $parameters Associatve parameter array.
   * @return \learnline\search\interfaces\View Returns itself.
   */
  public function setParameters($parameters);

  /**
   * Set a specific parameter.
   * Overrides existing values.
   *
   * @param  string $name  Parameter name.
   * @param  mixed  $value Parameter value.
   * @return \learnline\search\interfaces\View Returns itself.
   */
  public function setParameter($name, $value);

  /**
   * Formats the raw input data.
   *
   * @param  mixed $raw Raw input data.
   * @return \learnline\search\interfaces\View Returns itself.
   */
  public function format($raw);

  /**
   * Returns formatted/renderable array.
   *
   * @return array Returns renderable array.
   */
  public function getFormatted();

  /**
   * Render search result data array.
   *
   * @param  string $format Format to render.
   * @return \learnline\search\interfaces\View Returns itself.
   */
  public function render($format = null);

  /**
   * Returns the rendered output.
   *
   * @return string
   */
  public function getOutput();
} //interface
