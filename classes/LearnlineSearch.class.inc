<?php
// $id:$

/**
 * @file
 * Module's main class implementation.
 */

/**
 * LearnlineSearchDeactivatedException is thrown, when trying to use a
 * deactivated search service.
 */
class LearnlineSearchDeactivatedException extends \Exception {}

/**
 * class LearnlineSearch
 *
 * Perform search (and type-ahead) requests, fetch results, normalize them to
 * be compatible with each other (SODIS vs. EDMOND) and build a renderable
 * array from it.
 */
class LearnlineSearch {
  /**
   * Configuration variable names.
   */
  const SETTING__PERSISTENT_SETTINGS = 'learnline_search_settings_persistent';
  const SETTING__ADVANCED_SEARCH_OPTIONS = 'learnline_search_settings_search_options';
  const SETTING__LEGAL_NOTICE_HEADING = 'learnline_search_settings_results_legal_notice_heading';
  const SETTING__LEGAL_NOTICE = 'learnline_search_settings_results_legal_notice';
  const SETTING__RESULTS_PER_PAGE = 'learnline_search_settings_results_per_page';
  const SETTING__NO_RESULTS_FOUND = 'learnline_search_settings_no_results_found';
  const SETTING__CC_DIR = 'learnline_search_settings_cc_dir';

  /**
   * Bitwise arguments used to indicate, which
   * backend service(s) to use for search.
   */
  const SEARCH_ALL = 0xff;
  const SEARCH_SODIS = 0x01;
  const SEARCH_EDMOND = 0x02;

  /**
   * Recurring array keys.
   */
  const ACTIVE = '#active';
  const AVAILABLE = '#available';
  const DISPLAY_VALUE = '#display_value';
  const VALUES = '#values';
  const VALUE = '#value';
  const COUNT = '#count';
  const TITLE = '#title';
  const IS_ARRAY = '#is_array';
  const HREF = '#href';

  /**
   * Recurring array keys (espacially for parameter mapping).
   */
  const FILTER = 'filter';
  const SODIS = 'sodis';
  const EDMOND = 'edmond';
  const EDUTAGS = 'edutags';
  const PARAM = 'parameter';
  const HIDDEN = 'hidden';

  /**
   * Valid input parameter names.
   * Used for parameter mapping.
   *
   * @see $parameter_mapping
   * @see __construct()
   */
  const PARAM__SEARCH = 'search';
  const PARAM__EDMOND = 'edmond';
  const PARAM__PAGE = 'page';
  const PARAM__SIZE = 'size';
  const PARAM__FROM = 'from';
  const PARAM__SORT = 'sort';
  const PARAM__ORDER = 'order';
  const PARAM__KEYWORDS = 'keywords';
  const PARAM__LANGUAGES = 'languages';
  const PARAM__CONTEXTS = 'contexts';
  const PARAM__DISCIPLINES = 'disciplines';
  const PARAM__CONTENTTYPES = 'contenttypes';
  const PARAM__PUBLISHERS = 'publishers';
  const PARAM__PROVIDERS = 'providers';
  const PARAM__COPYRIGHTS = 'copyrights';
  const PARAM__COMPETENCIES = 'competencies';

  /**
   * Instance of class LearnlineSearch.
   * (Implementing a singleton pattern)
   *
   * @var \LearnlineSearch
   */
  private static $instance;

  /**
   * Array of configuration settings.
   *
   * @var array
   */
  private static $settings;

  /**
   * This module's path.
   *
   * @var string
   */
  protected $path;

  /**
   * Configured maximum count for type-ahead/auto-completion suggestions.
   *
   * @var int
   */
  protected $type_ahead_count;

  /**
   * Decoded JSON result of a search request to SODIS.
   *
   * @var array
   */
  protected $result = array(
    'error' => array(),
  );

  /**
   * Formatted result data as renderable array.
   *
   * @var array
   */
  protected $formatted = array();

  /**
   * Simple list of available facets for internal use
   * @see LearnlineSearch::parseSodisFacets()
   *
   * @var type
   */
  private $available_facets = array();

  /**
   * Parameter mapping.
   *
   * @var array
   */
  protected $parameter_mapping;

  public $test = array();

  /**
   * Call LearnlineSearch::getInstance() instead of using the constructor.
   */
  protected function __construct() {
    $this->parameter_mapping = array(
      self::PARAM__SEARCH => array(
        self::TITLE => t('Search term'),
        self::SODIS => array(
          self::PARAM => 'q',
        ),
      ),
      self::PARAM__EDMOND => array(
        self::TITLE => t('EDMOND identity'),
        self::IS_ARRAY => TRUE,
        self::EDMOND => array(
          self::PARAM => '%identity',
        ),
      ),
      self::PARAM__PAGE => array(
      ),
      self::PARAM__SIZE => array(
        self::SODIS => TRUE,
      ),
      self::PARAM__FROM => array(
        self::SODIS => TRUE,
      ),
      self::PARAM__SORT => array(
        self::TITLE => t('Sorting criteria'),
        self::SODIS => TRUE,
      ),
      self::PARAM__ORDER => array(
        self::TITLE => t('Sort order'),
        self::SODIS => TRUE,
      ),
      self::PARAM__CONTEXTS => array(
        self::TITLE => t('Resource context'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'context',
          'll3:context'
        ),
      ),
      self::PARAM__DISCIPLINES => array(
        self::TITLE => t('Disciplines'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'discipline',
          array('lom:classificationDisciplineId' => 'lom:classificationDisciplineEntry'),
          array('ll3:classificationDisciplineId' => 'll3:classificationDisciplineEntry'),
        ),
        self::EDMOND => array(
          self::PARAM => 'geb',
        ),
      ),
      self::PARAM__CONTENTTYPES => array(
        self::TITLE => t('Media types'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'learningResourceType',
          'll3:learningResourceType',
        ),
        self::EDMOND => array(
          self::PARAM => 'typen',
        ),
      ),
      self::PARAM__PUBLISHERS => array(
        self::TITLE => t('Publishers'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'publisher',
          'lom:publisher'
        ),
      ),
      self::PARAM__COPYRIGHTS => array(
        self::TITLE => t('Copyright'),
        self::IS_ARRAY => TRUE,
        self::FILTER => function ($input) {
          return strtoupper($input);
        },
        self::SODIS => array(
          self::PARAM => 'copyright',
          'lom:rights',
        ),
      ),
      self::PARAM__LANGUAGES => array(
        self::TITLE => t('Languages'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'language',
          'lom:general_language'
        ),
        self::EDMOND => array(
          self::PARAM => 'sprache',
        ),
      ),
      self::PARAM__KEYWORDS => array(
        self::TITLE => t('Keywords'),
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'keyword',
          'lom:general_keyword'
        ),
        self::EDMOND => array(
          self::PARAM => 'schlag',
        ),
      ),
      self::PARAM__PROVIDERS => array(
        self::TITLE => t('Providers'),
        self::HIDDEN => TRUE,
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'provider',
        ),
      ),
      self::PARAM__COMPETENCIES => array(
        self::TITLE => t('Competencies'),
        self::HIDDEN => TRUE,
        self::IS_ARRAY => TRUE,
        self::SODIS => array(
          self::PARAM => 'competency',
        ),
      ),
    );
  } //function

  /**
   * Implements a replacement for the constructor, considering the singleton
   * pattern.
   *
   * @return \LearnlineSearch Returns itself.
   */
  public static function getInstance() {
    if (!isset(self::$instance)) {
      self::$instance = new LearnlineSearch();
    } //if

    foreach (array(
        self::SETTING__PERSISTENT_SETTINGS => null,
        self::SETTING__ADVANCED_SEARCH_OPTIONS => null,
        self::SETTING__RESULTS_PER_PAGE => null,
        self::SETTING__LEGAL_NOTICE_HEADING => null,
        self::SETTING__LEGAL_NOTICE => null,
        self::SETTING__CC_DIR => '/sites/default/files/images/cc/',
    ) as $setting => $default_value) {
      if ((self::$settings[$setting] = variable_get($setting, $default_value)) === null) {
        throw new RuntimeException(
            t("%class is missing setting '%setting'.", array(
                '%class' => get_called_class(),
                '%setting' => $setting)));
      }
    } //foreach

    return self::$instance;
  } //function

  public static function getSetting($setting_variable_name) {
    self::getInstance();
    if (!isset(self::$settings[$setting_variable_name])) {
      throw new Exception(t("%class has no setting named '%setting'.", array(
          '%class' => get_called_class(),
          '%setting' => $setting_variable_name)));
    } //if
    return self::$settings[$setting_variable_name];
  } //function

  /**
   * Converts a query array into a query string.
   *
   * @param array   $queryArray Query array as retrieved from drupal_get_query_array().
   * @return string             The appropriate query string starting with '?'.
   */
  public function queryToString($queryArray) {
    $query = array();
    foreach ($queryArray as $key => $value) {
      if ($key === 'q') {
        // We will remove the q parameter, because it is reserved by Drupal to
        // keep the current request path.
        continue;
      } //if

      if (is_array($value)) {
        foreach ($value as $arr_value) {
          $query[] = $key . '[]=' . urlencode($arr_value);
        } //foreach
      } //if
      else {
        $query[] = $key . '=' . urlencode($value);
      } //else
    } //foreach

    return '?' . implode('&', $query);
  } //function

  /**
   * Returns the human readable title for a facet.
   *
   * @param string $facet_name
   * @return string
   * @throws Exception
   */
  public function getFacetTitle($facet_name) {
    if (!isset($this->parameter_mapping[$facet_name])) {
      throw new Exception("Unknown facet '$facet_name'.");
    }
    elseif (!isset($this->parameter_mapping[$facet_name][self::TITLE])) {
      throw new Exception("Unknown title for facet '$facet_name'.");
    }
    return $this->parameter_mapping[$facet_name][self::TITLE];
  } //function

  /**
   * Filters a facet's value according to its parameter mapping configuration.
   *
   * @param string $facet_name
   * @param string $input
   * @return string
   */
  public function facetFilter($facet_name, $input) {
    if (isset($this->parameter_mapping[$facet_name]) &&
        isset($this->parameter_mapping[$facet_name][self::FILTER]) &&
        is_callable($this->parameter_mapping[$facet_name][self::FILTER])) {
      return call_user_func(
          $this->parameter_mapping[$facet_name][self::FILTER],
          $input);
    } //if
    return $input;
  } //function

  /**
   * Check whether a facet can receive multiple arguments or not.
   *
   * @param string $facet_name
   * @return bool
   */
  public function isMultiFacet($facet_name) {
    return isset($this->parameter_mapping[$facet_name][self::IS_ARRAY])
        && $this->parameter_mapping[$facet_name][self::IS_ARRAY];
  } //function

  /**
   * Returns the module path.
   *
   * @return string Relative path to the module directory.
   */
  public function getModulePath() {
    if (!isset($this->path)) {
      $this->path = drupal_get_path('module', 'learnline_search');
    } //if

    return $this->path;
  } //function

  /**
   * Returns the parameter mapping array.
   *
   * @return array
   */
  public function getParameterMapping() {
    return $this->parameter_mapping;
  } //function

  public static function getCacheExpirationTime() {
    return time() + 1800;
  } //function
} //class
