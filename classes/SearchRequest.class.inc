<?php
// $id:$

/**
 * @file
 * This file contains an abstract Request class implementation.
 */

namespace learnline\search\classes;

use learnline\search\interfaces\SearchRequest as SearchRequestIntf;

abstract class SearchRequest implements SearchRequestIntf {
  /**
   * Instance of Request class, used in self::create() for cloning.
   *
   * @var \learnline\search\classes\SearchRequest
   */
  protected static $instance = array();

  /**
   * Service identifier.
   *
   *  LearnlineSearch::SODIS
   *  LearnlineSearch::EDMOND
   *
   * @var string
   */
  protected $service;

  /**
   * Request target/URL.
   *
   * @var string
   */
  protected $url = '';

  /**
   * Request method.
   *
   * @var self::GET|self::POST
   */
  protected $method;

  /**
   * Request headers as associative array.
   *
   * @var array|FALSE
   */
  protected $headers = FALSE;

  /**
   * Request body.
   *
   * @var string
   */
  protected $body = '';

  /**
   * Request body as associatve array, but only if data was derived through
   * $this->setData().
   *
   * @var array|FALSE
   */
  protected $body_data = FALSE;

  /**
   * Request execution timeout
   *
   * @var float
   */
  protected $timeout = self::DEFAULT__EXEC_TIMEOUT;

  /**
   * The maximum number of accepted redirects.
   *
   * @var int
   */
  protected $max_redirects = self::DEFAULT__MAX_REDIRECTS;

  /**
   * The stream context resource, if set.
   *
   * @var resource|NULL
   */
  protected $context = FALSE;

  /**
   * Raw response retrieved from drupal_http_request.
   *
   * @var \stdClass|FALSE
   */
  protected $raw_response = FALSE;

  /**
   * The corresponding Response object.
   *
   * @var \learnline\search\classes\http\Response|FALSE
   */
  protected $response = FALSE;

  /**
   * Returns a new Request.
   *
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public static function create() {
    $class = get_called_class();
    if (!isset(self::$instance[$class])) {
      self::$instance[$class] = new $class();
      return self::$instance[$class];
    } //if

    $new = clone self::$instance[$class];
    return $new->reset();
  } //function

  /**
   * Unsets all values of the current request.
   *
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function reset() {
    $this->body = $this->url = '';
    $this->body_data = $this->context = $this->headers = FALSE;
    return $this->setMethod()->setMaxRedirects()->setTimeout();
  } //function

  /**
   * Returns the stream context resource if set, otherwise FALSE.
   *
   * @return resource|FALSE
   */
  public function getStreamContext() {
    return $this->context;
  } //function

  /**
   * Sets a resource context.
   *
   * A resource context can be produced using PHP's native function
   * stream_context_create().
   *
   * @param  resource $context A stream context resource.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setStreamContext($context) {
    $this->context = $context;
    return $this;
  }

  /**
   * Returns the URL to which the request will be staged.
   *
   * @return string|FALSE
   */
  public function getUrl() {
    return $this->url;
  } //function

  /**
   * Set the request target URL.
   *
   * @param  string $url URL to which to stage the request.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setUrl($url) {
    assert('is_string($url)');
    $this->url = $url;
    return $this;
  } //function

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   */
  public function getHeaders() {
    return $this->headers;
  } //function

  /**
   * Set the HTTP headers as associative array.
   *
   * @param  array $headers Associative array containing the request headers.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setHeaders($headers) {
    assert('is_array($headers)');
    $this->headers = $headers;
    return $this;
  } //function

  /**
   * Returns the request method.
   *
   * @return string
   */
  public function getMethod() {
    return $this->method;
  } //function

  /**
   * Sets the request method to use.
   *
   * @param  string $method Either self::GET or self::POST.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setMethod($method = self::METHOD__GET) {
    assert('$method === \learnline\search\classes\SearchRequest::METHOD__GET || ' .
        '$method === \learnline\search\classes\SearchRequest::METHOD__POST');
    $this->method = $method;
    return $this;
  } //function

  /**
   * Returns the maximum execution time of the request.
   *
   * @return float
   */
  public function getTimeout() {
    return (float) $this->timeout;
  } //function

  /**
   * Set the request timeout.
   *
   * This timeout takes effect no matter if a response is received or not: When
   * the timeout exceeds, the request is canceled.
   *
   * @param  float $seconds Timeout.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setTimeout($seconds = self::DEFAULT__EXEC_TIMEOUT) {
    assert('is_numeric($seconds)');
    $this->timeout = (float) $seconds;
    return $this;
  } //function

  /**
   * Returns the maximum number of redirects accepted.
   *
   * @return int
   */
  public function getMaxRedirects() {
    return (int) $this->max_redirects;
  } //function

  /**
   * Defines the maximum acceptable count of redirects.
   *
   * @param  int $count Number of accepted redirects.
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setMaxRedirects($count = self::DEFAULT__MAX_REDIRECTS) {
    assert('is_numeric($count)');
    $this->max_redirects = (int) $count;
    return $this;
  } //function

  /**
   * Returns the resquest data/body.
   *
   * @return array|FALSE
   */
  public function getData() {
    return $this->body_data;
  } //function

  /**
   * Takes an associative array, puts it into URL-encoded format and sets the
   * result as request body.
   *
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setData($data) {
    assert('is_array($data)');

    $this->body = $this->urlEncode($this->body_data = $data);
    return $this;
  } //function

  /**
   * Returns the raw request body.
   *
   * @return string|FALSE
   */
  public function getRawData() {
    return $this->body;
  } //function

  /**
   * Takes data as it is for the request body.
   *
   * @return \learnline\search\classes\SearchRequest Returns itself.
   */
  public function setRawData($data) {
    assert('is_string($data)');
    $this->body = $data;
    return $this;
  } //function

  /**
   * Get the originating HTTP request as raw string.
   *
   * @return string
   */
  public function getResponse() {
    return $this->response;
  } //function

  /**
   * Returns the raw response if available.
   *
   * @return \stdClass|FALSE
   */
  public function getRawResponse() {
    return $this->raw_response;
  } //function

  /**
   * Returns an array containing all results.
   * Array keys are the appropriate result IDs.
   *
   * @return \learnline\search\classes\SearchResult[] The actual results.
   */
  public function getResults() {
    return $this->response->getResults();
  } //function

  /**
   * Returns the result specified by its index.
   *
   * @param  string $index Result ID.
   * @return \learnline\search\classes\SearchResult The search result with ID $index.
   */
  public function getResult($index) {
    return $this->response->getResult($index);
  } //function

  /**
   * Sends request and receives response.
   *
   * @return \learnline\search\classes\SearchRequest
   */
  public function send() {
    $responseClassFactory = '\learnline\search\classes\\' . $this->service .
        '\Response::create';

    $this->raw_response = drupal_http_request($this->getUrl(), $this->getOptions());

    if (isset($this->raw_response->error) && !empty($this->raw_response->error)) {
      throw new \RuntimeException(t('@service returned error code @code: %msg', array(
          '@service' => $this->service,
          '@code' => $this->raw_response->code,
          '%msg' => $this->raw_response->error,
        )),
        $this->raw_response->code
      );
    }

    $this->response = call_user_func($responseClassFactory, $this);
    return $this;
  } //function

  /**
   * Builds an options array based on the object's attributes, ready for use
   * with drupal_http_request().
   *
   * @return array
   */
  protected function getOptions() {
    $options = array(
      'max_redirects' => $this->max_redirects,
      'method' => $this->method,
      'timeout' => $this->timeout,
    );

    if ($this->body) {
      $options['data'] = $this->body;
    } //if
    if ($this->context) {
      $options['context'] = $this->context;
    } //if
    if ($this->headers) {
      $options['headers'] = $this->headers;
    } //if

    return $options;
  } //function
} //class
