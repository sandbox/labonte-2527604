<?php

// $id:$

/**
 * @file
 * This file extends the SearchResultBase class to a SODIS specific form.
 */

namespace learnline\search\classes\edmond;

use learnline\search\classes\SearchResult;
use LearnlineSearch;

class Result extends SearchResult {
  /**
   * Parameter: Resource URI.
   */
  const RESOURCE = 'resource';

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = LearnlineSearch::EDMOND;

  /**
   * Associative array, defining the theme to use for each render format.
   *
   * @var string[]
   */
  protected $theme = 'learnline_search_result_edmond';

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $format_structure = array(
    'nr' => '#nr',
    'titel' => '#title',
    'text' => '#text',
    'typ' => '#type',
    'herausgeber' => '#publisher',
    'prodjahr' => '#year',
  );

  protected $format_callbacks = array(
    '#type' => 'self::type',
  );

  /**
   * Field mappings.
   * Used to translate the cryptic integer representation of some data fields.
   *
   * @var array
   */
  public static  $fieldmap = array(
    'typ' => array(
      29 => 'Audio',
      49 => 'Video',
      55 => 'Scorm',
      19 => 'Bild',
      79 => 'Dokument',
      58 => 'Internetseite',
      69 => 'Programm',
    ),
  );

  public function format($raw = null) {
    assert('$raw instanceof \SimpleXMLElement');
    $formatted = array(
      '#theme' => $this->theme,
    );

    if (!isset($raw)) {
      $raw = $this->raw;
    } //if

    // Extract data defined via $this->format_structure.
    foreach ($raw as $element) {
      $attribute = (string) $element['n'];
      $value = (string) $element;
      
      if (isset($this->format_structure[$attribute])) {
        $var = $this->format_structure[$attribute];
        $str_val = isset($this->format_callbacks[$var]) &&
            is_callable($this->format_callbacks[$var])
            ? call_user_func($this->format_callbacks[$var], $value)
            : (string) $value;

        $formatted[$var] = $str_val;
      } //if
    } //foreach
    
    $formatted['#id'] = isset($this->raw['identifier'])
        ? $this->raw['identifier']
        : $formatted['#nr'];

    $formatted['#href'] = 'http://edmond20.lvr.de/rec?src=online&' .
        'rtmpl=edmondlogin&id=' . (string) $formatted['#id'];
    
    if ($mz = $this->getEdmondLocation()) {
      $formatted['#href'] .= '&standort=' . $mz;
    } //if

    return $formatted;
  } //function

  /**
   * Returns the resource location/URI as string.
   *
   * @return type
   */
  public function getResourceLocation() {
    throw new \Exception('NOT YET IMPLEMENTED!');
  } //function

  /**
   * Returns filtered copyright values.
   *
   * @param  string $input String to be filtered.
   * @return string        Returns the filtered string.
   */
  public static function type($input) {
    $int = (int) $input;
    return isset(self::$fieldmap['typ'][$int])
        ? self::$fieldmap['typ'][$int] : '';
  } //function

  /**
   * Returns the service identifier.
   *
   * @return string
   */
  public function getServiceId() {
    return isset($this->formatted['#id']) ? $this->formatted['#id'] : FALSE;
  } //fucntion
  
  /**
   * Returns the (first) selected EDMOND "Medienzentrum" or FALSE.
   * 
   * @return string|FALSE
   */
  public function getEdmondLocation() {
    static $location = null;
    
    if (!isset($location)) {
      $params = drupal_get_query_parameters();
      $location = isset($params[LearnlineSearch::PARAM__EDMOND]) &&
          !empty($params[LearnlineSearch::PARAM__EDMOND])
          ? $params[LearnlineSearch::PARAM__EDMOND]
          : FALSE;

      if (is_array($location)) {
        $location = (string) reset($location);
      } //if
    } //if
    
    return $location;
  } //function
} //class
