<?php

// $id:$

/**
 * @file
 * EdmondSearchService implementation.
 */

namespace learnline\search\classes\edmond;

use learnline\search\classes\SearchService;
use XMLWriter;

class Service extends SearchService {
  const IDENTIFIER = 'edmond';
  const CACHE = 'cache_learnline_edmond';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'learnline_search_settings_edmond_search';
  const SETTING__ROOT_IDENTITY = 'learnline_search_settings_edmond_root_identity';
  const SETTING__PASSWORD = 'learnline_search_settings_edmond_password';
  const SETTING__MAX_RESULTS = 'learnline_search_settings_edmond_max_results';
  const SETTING__SEARCH_URI = 'learnline_search_settings_edmond_search_uri';
  const SETTING__LOGO = 'learnline_search_settings_edmond_logo';
  
  protected static $identity;

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @var array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => null,
      self::SETTING__ROOT_IDENTITY => null,
      self::SETTING__PASSWORD => null,
      self::SETTING__MAX_RESULTS => 10,
      self::SETTING__SEARCH_URI => null,
      self::SETTING__LOGO => null,
    );
  } //function

  /**
   * Shorthand method for requesting SODIS.
   *
   * @param  array $parameters
   * @return \learnline\search\classes\http\Response
   */
  public static function search($parameters) {
    $request = Request::create()->setParameters($parameters);
    $cid = md5($request->getUrl() . $request->getRawData());

    if ($cached = cache_get($cid, self::CACHE)) {
      return $cached->data;
    } //if

    $request->send();
//    cache_set($cid, $request, self::CACHE,
//        LearnlineSearch::getCacheExpirationTime());

    return $request;
  } //function

  /**
   * Implements SearchService->getResourceLocation().
   * @todo Either finish this method implementation or remove it...
   */
  public static function getResourceLocation($resource_id) {
    try {
      $url = Request::create()->formatQueryUrl(drupal_get_query_parameters());

      $xml = new XMLWriter();
      $xml->openMemory();
      $xml->startElement('record');
        $xml->writeAttribute('identifier', $resource_id);
        $xml->writeAttribute('template', 'edmond_superplain');
      $xml->endElement();
      
      $response = drupal_http_request($url, array(
        'method' => 'POST',
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
        'data' => 'xmlstatement=' . urlencode($xml->outputMemory(FALSE)),
      ));

      return $response->data;
    } catch (Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    } //try/catch
  } //function
} //class
