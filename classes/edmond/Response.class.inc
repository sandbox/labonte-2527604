<?php
// $id:$

/**
 * @file
 * This file contains the SODIS specific implementation of the \SearchResponse
 * interface.
 */

namespace learnline\search\classes\edmond;

use learnline\search\classes\SearchResponse;
use LearnlineSearch;

class Response extends SearchResponse {
  protected $service = LearnlineSearch::EDMOND;
  protected $theme = 'learnline_search_results_edmond';

  protected function parseData() {
    assert('isset($this->raw->data)');
    libxml_use_internal_errors(TRUE);
    $this->data = simplexml_load_string($this->raw->data);

    if (libxml_get_last_error() || !isset($this->data)) {
      throw new \RuntimeException(
          t('Underlying search service responded with invalid data portion.'));
    } //if

    libxml_use_internal_errors();
    $this->data->addAttribute('total', count($this->data->children()));
    $this->data->addAttribute('identity', $this->request->getIdentity());
  } //function

  protected function extractResults() {
    if (!($this->data instanceof \SimpleXMLElement)) {
      throw new \RuntimeException(
          t('Got strange data portion for a SODIS response.'));
    } //if

    $array = array();
    foreach ($this->data->children() as $child) {
      $array[] = $child;
    }
    return $array;
  } //function

  public function format($raw = null) {
    global $base_url;
    $formatted = array(
      '#ajax' => isset($_GET['ajax']) ? !!$_GET['ajax'] : FALSE,
      '#theme' => $this->theme,
      '#results' => $this->getResults(),
      '#max_display' => Service::getSetting(Service::SETTING__MAX_RESULTS),
      '#total' => isset($this->data['total']) ? $this->data['total'] : 0,
      '#href' => $base_url . '/edmond/search?' . $_SERVER['QUERY_STRING'],
      '#logos' => Service::getSetting(Service::SETTING__LOGO),
      '#span_width' => floor(100 /
          (Service::getSetting(Service::SETTING__MAX_RESULTS) +1)),
    );

    if (!isset($this->data) || empty($this->data) || isset($this->data['error'])) {
      $formatted['#error'] = $this->data['error'];
    } //if

    return $formatted;
  } //function

  public function getFormattedTeaser() {
    $formatted = $this->formatted;
    $formatted['#theme'] = 'learnline_search_results_edmond_teaser';

    if (!isset($formatted['#results']) || !is_array($formatted['#results'])) {
      $formatted['#results'] = array();
    } //if

    return $formatted;
  } //fucntion
} //interface
