<?php
// $id:$

/**
 * @file
 * This file contains the Request class interface.
 */

namespace learnline\search\classes\edmond;

use learnline\search\classes\SearchRequest;
use \LearnlineSearch;

class Request extends SearchRequest {
  protected $service = LearnlineSearch::EDMOND;
  protected $method = self::METHOD__POST;
  protected $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
  );

  /**
   * Stores parameters in case of calling $this->setParameters().
   *
   * @var array
   */
  protected $parameters = array();

  protected $identity;

  /**
   * Simply assign the query parameters to get a ready-to-send configured
   * Request object.
   *
   * @param  array $parameters
   * @return \learnline\search\classes\sodis\Request
   */
  public function setParameters($parameters) {
    $this->parameters = $parameters;
    $this->setUrl($this->formatQueryUrl($parameters))
        ->setRawData('xmlstatement=' . $this->formatQueryData($parameters));

    return $this;
  } //function

  public function setParametersForLicenseRequest($parameters, $ids) {
    $this->parameters = $parameters;
    // Instanciate XMLWriter:
    $xml = new \XMLWriter();
    $xml->openMemory();
    $xml->startElement('search');
      $xml->writeAttribute('noliz', '1');
      $xml->startElement('condition');
        $xml->writeAttribute('operator', 'OR');
        $xml->writeAttribute('option', 'WORD');
        $xml->writeAttribute('field', 'nr');
        $xml->writeRaw(implode(' ', $ids));
      $xml->endElement();
    $xml->endElement();

    $this->setUrl($this->formatQueryUrl($parameters))
        ->setRawData('xmlstatement=' . $xml->outputMemory(FALSE));

    return $this;
  }

  /**
   * Returns parameters after usage of $this->setParameters(); otherwise an
   * empty array will be returned.
   *
   * @return array
   */
  public function getParameters() {
    return $this->parameters;
  } //function

  /**
   * Formats the URL for our XML query to stage.
   *
   * @param   array   $parameters   Query parameter array.
   * @return  string                The request URI including the FQDN.
   */
  public function formatQueryUrl($parameters) {
    $identities = array();

    if (isset($parameters[LearnlineSearch::PARAM__EDMOND]) &&
        is_array($parameters[LearnlineSearch::PARAM__EDMOND])) {
      foreach ($parameters[LearnlineSearch::PARAM__EDMOND] as $identity) {
        if (count($parameters[LearnlineSearch::PARAM__EDMOND]) === 1) {
          $this->identity = $identity;
        } //if
        $identities[] = Service::getSetting(Service::SETTING__ROOT_IDENTITY) . '/' . $identity;
      }
    } //if
    elseif (isset($parameters[LearnlineSearch::PARAM__EDMOND]) &&
        is_string($parameters[LearnlineSearch::PARAM__EDMOND])) {
      $this->identity = $parameters[LearnlineSearch::PARAM__EDMOND];
      $identities[] = Service::getSetting(Service::SETTING__ROOT_IDENTITY) . '/' .
          $parameters[LearnlineSearch::PARAM__EDMOND];
    } //elseif
    else {
      $identities[] = Service::getSetting(Service::SETTING__ROOT_IDENTITY);
    } //else

    $url = Service::getSetting(Service::SETTING__SEARCH_URI);
    substr(Service::getSetting(Service::SETTING__SEARCH_URI), -1) === '/' || $url .= '/';
    isset($identities) && $url .= implode(',', $identities);

    return $url;
  } //function

  /**
   * Formats the XML query string according to the given request parameters.
   *
   * @param   array   $parameters   Query parameter array.
   * @return  string                The XML formatted query statement.
   */
  public function formatQueryData($parameters) {
    $searchword = isset($parameters[LearnlineSearch::PARAM__SEARCH])
        ? (string) $parameters[LearnlineSearch::PARAM__SEARCH] : '';
    
    // Prepare search term.
    // As long as EDMOND won't accept HTML-Entity-, URL- or Un-encoded special
    // chars like '<' and '>', we simply have to strip off these characters.
    $searchword = str_replace(array('<', '>'), '', $searchword);

    // Instanciate XMLWriter:
    $xml = new \XMLWriter();
    $xml->openMemory();

    // Set indent for a better readability when debugging:
//    $xml->setIndent(4);

    // Build request:
//    $xml->startDocument('1.0', 'UTF-8');
    $xml->startElement('search');
      $xml->writeAttribute('fields', 'nr,titel,text,typ,prodjahr,herausgeber');
      if (!empty($searchword)) {
        $xml->startElement('condition');
          $xml->writeAttribute('field', 'text_fields');
          $xml->writeRaw($searchword);
        $xml->endElement();
      }
    $xml->endElement();

    return $xml->outputMemory(FALSE);
  } //function

  public function getIdentity() {
    return $this->identity;
  } //function
  
  public function setData($plain_data_string) {
    $this->body = $this->body_data = $plain_data_string;
    return $this;
  } //function
} //class
