<?php

// $id:$

/**
 * @file
 * Main module file.
 */

namespace learnline\search\classes;

use learnline\search\interfaces\SearchService as SearchServiceIntf;

abstract class SearchService implements SearchServiceIntf {

  const VALUE = 'value';

  const IS_SYNC = 'sync';

  /**
   * Associative array containing setting identifiers and default values.
   *
   * @var array
   */
  protected static $settings;

  /**
   * The actual settings.
   *
   * @var array
   */
  protected static $data = array();

  /**
   * Class instance.
   *
   * @var \learnline\search\classes\SearchService
   */
  protected static $instance;

  protected function __construct(&$class) {
    self::$settings = $class::getDefaultSettings();

    if (!isset(self::$settings)) {
      throw new \Exception(t('%class is missing settings.',
          array('%class' => $class)));
    } //if

    foreach (self::$settings as $name => $default_value) {
      assert('is_string($name)');
      if (isset($default_value)) {
        continue;
      } //if

      self::$data[$name] = array(
        self::VALUE => variable_get($name, $default_value),
        self::IS_SYNC => TRUE,
      );

      if (!isset(self::$data[$name][self::VALUE])) {
        throw new \RuntimeException(t("Missing required setting '%setting'.",
            array('%setting' => $name)));
      } //if
    } //foreach
  } //function

  public function __destruct() {
    $this->save();
  } //function

  /**
   * Initialization.
   *
   * Internally used as first call in all static methods, to verify that all.
   */
  public static function init() {
    if (!isset(self::$instance)) {
      $class = get_called_class();
      self::$instance = new $class($class);
    } //if

    return self::$instance;
  } //function

  /**
   * Returns all settings as associative array.
   *
   * @return array
   */
  public static function getSettings() {
    self::init();
    $settings = array();
    foreach (self::$settings as $name => $default_value) {
      if (isset(self::$data[$name])) {
        $settings[$name] = self::$data[$name][self::VALUE];
      } //if

      $settings[$name] = self::loadSetting($name);
    } //foreach

    return $settings;
  } //function

  /**
   * Saves multiple settings at once.
   *
   * @param array $settings Associative array containing multiple settings.
   * @return \learnline\search\interfaces\SearchService Returns itself.
   * @throws \RuntimeException
   */
  public static function setSettings($settings) {
    assert('is_array($settings)');
    foreach ($settings as $name => $value) {
      self::setSetting($name, $value);
    } //foreach

    return self::$instance;
  } //function

  /**
   * Returns the specified setting's value.
   *
   * @param  string $setting Variable/Setting identifier.
   * @return mixed
   * @throws \RuntimeException
   */
  public static function getSetting($setting) {
    self::init();
    if (!isset(self::$data[$setting])) {
      return self::loadSetting($setting);
    } //if

    return self::$data[$setting][self::VALUE];
  } //function

  /**
   * Save a setting.
   *
   * @param  string $setting Setting name.
   * @param  mixed $value Value to set.
   * @return \learnline\search\interfaces\SearchService Returns itself.
   * @throws \RuntimeException
   */
  public static function setSetting($setting, $value) {
    self::$data[$setting] = array(
      self::VALUE => $value,
      self::IS_SYNC => FALSE,
    );

    return self::instance;
  } //function

  /**
   * Load setting from database.
   *
   * This method doesn't care if the variable has already been loaded or about
   * its sync status.
   *
   * @param string $setting
   * @return mixed
   */
  public static function loadSetting($setting) {
    self::init();
    self::$data[$setting] = array(
      self::VALUE => variable_get($setting, isset(self::$settings[$setting])
          ? self::$settings[$setting] : NULL),
      self::IS_SYNC => TRUE,
    );

    return self::$data[$setting][self::VALUE];
  } //function

  /**
   * Store current settings to database.
   *
   * @return \learnline\search\classes\SearchService
   */
  public static function save() {
    self::init();
    foreach (self::$data as $name => &$setting) {
      if ($setting[self::IS_SYNC]) {
        continue;
      } //if

      variable_set($name, $setting[self::VALUE]);
      $setting[self::IS_SYNC] = TRUE;
    } //foreach

    return self::$instance;
  } //function
} //class
