<?php
// $id:$

/**
 * @file
 * This file contains the SearchResultBase class implementing the SearchResult
 * interface.
 */

namespace learnline\search\classes;

use learnline\search\interfaces\View;
use LearnlineSearch;

class HtmlView implements View {
  /**
   * View's default render format.
   *
   * @var string
   */
  protected $default_format = self::FORMAT__HTML;

  /**
   * Associative array, defining the theme to use for each render format.
   *
   * @var string|NULL
   */
  protected $theme = NULL;

  /**
   * Parameters.
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Raw result data.
   *
   * @var mixed
   */
  protected $raw;

  /**
   * Formatted Renderable array.
   *
   * @var array
   */
  protected $formatted;

  /**
   * Rendered output data.
   *
   * @var mixed
   */
  protected $output;

  /**
   * Object instances for cloning/factory implementation.
   *
   * @var \learnline\search\classes\SearchResult[]
   */
  protected static $instance = array();

  public function __construct() {
    $this->parameters[self::FORMAT] = $this->default_format;
  } //function

  /**
   * Returns a new SearchResult.
   *
   * @param  mixed $raw Raw input data.
   * @return \learnline\search\classes\SearchResult Returns itself.
   */
  public static function create($raw) {
    $class = get_called_class();

    if (!isset(self::$instance[$class])) {
      self::$instance[$class] = new $class();
      return self::$instance[$class]->parse($raw);
    } //if

    $new = clone self::$instance[$class];
    return self::$instance[$class] = $new->parse($raw);
  } //function

  /**
   * Parse raw result data.
   *
   * @param  mixed $raw Raw input data.
   * @return \learnline\search\classes\SearchResult Returns itself.
   */
  public function parse($raw) {
    $this->formatted = $this->format($this->raw = $raw);
    return $this;
  } //function

  /**
   * Formats the raw input data.
   *
   * @param  mixed $raw Raw input data.
   * @return array      Returns renderable formatted array.
   */
  public function format($raw = null) {
    $formatted = array();

    if (!isset($raw)) {
      $raw = $this->raw;
    } //if

    if (is_string($raw)) {
      $formatted = array('#markup' => $raw);
    } //if
    elseif (is_array($raw)) {
      $formatted = $raw;

      if (!isset($formatted['#theme']) && isset($this->theme)) {
        $formatted['#theme'] = $this->theme;
      } //if
    } //if

    return $formatted;
  } //function

  /**
   * Returns formatted/renderable array.
   *
   * @return array
   */
  public function getFormatted() {
    return $this->formatted;
  } //function

  /**
   * Set parameters.
   * Overrides existing values.
   *
   * @param  array $parameters Associatve parameter array.
   * @return \learnline\search\classes\SearchResult Returns itself.
   */
  public function setParameters($parameters) {
    assert('is_array($parameters)');

    $this->parameters = $parameters;
    return $this;
  } //function

  /**
   * Set a specific parameter.
   * Overrides existing values.
   *
   * @param  string $name      Parameter name.
   * @param  mixed  $value     Parameter value.
   * @return \learnline\search\classes\SearchResult Returns itself.
   */
  public function setParameter($name, $value) {
    assert('is_string($name)');

    $this->parameters[$name] = $value;
    return $this;
  } //function

  /**
   * Render search result data array.
   *
   * @param  string $format    Format to render.
   * @return \learnline\search\classes\SearchResult Returns itself.
   */
  public function render($format = null) {
    assert('isset($this->formatted)');
    assert('is_array($this->formatted)');

    if (!isset($format)) {
      $format = $this->parameters[self::FORMAT];
    } //function

    $renderer = array($this, 'render' . ucfirst($format));
    assert('is_callable($renderer)');

    $this->output = call_user_func($renderer);

    return $this;
  } //function

  /**
   * Renders to HTML format.
   *
   * @return string Rendered HTML string.
   */
  public function renderHtml() {
    return drupal_render($this->formatted);
  } //function

  /**
   * Returns the rendered output.
   *
   * @return string
   */
  public function getOutput() {
    if (!isset($this->output)) {
      $this->render();
    } //if
    
    assert('is_string($this->output)');
    return $this->output;
  } //function

  /**
   * Implements IteratorAggregate.
   *
   * @return \ArrayIterator
   */
  public function getIterator() {
    return new ArrayIterator($this->formatted);
  } //function

  /**
   * Implements magic __set() method.
   *
   * @param  string $name  Key in the renderable data array.
   * @param  mixed  $value Value to set.
   * @throws RuntimeException
   */
  public function __set($name, $value) {
    $this->formatted[$name] = $value;
  } //function

  /**
   * Implements magic __get() method.
   *
   * @param  string $name Key in the renderable data array.
   * @return mixed        The value.
   * @throws RuntimeException
   */
  public function __get($name) {
    if (array_key_exists($name, $this->formatted)) {
      return $this->formatted[$name];
    } //if
    else {
      throw new \RuntimeException(
          t("There is no data item '@name'.", array('@name' => $name)));
    } //else
  } //function

  public function offsetExists($offset) {
    return array_key_exists($offset, $this->formatted);
  } //function

  public function offsetGet($offset) {
    return array_key_exists($offset, $this->formatted)
        ? $this->formatted[$offset]
        : NULL;
  }

  public function offsetSet($offset, $value) {
    if (!isset($offset)) {
      $this->formatted[] = $value;
    }
    else {
      $this->formatted[$offset] = $value;
    }
  }

  public function offsetUnset($offset) {
    if (array_key_exists($offset, $this->formatted)) {
      unset($this->formatted[$offset]);
    }
  }

  /**
   * URL-encodes an associative array.
   *
   * @param array $data
   * @return string
   */
  public function urlEncode($data) {
    $parts = array();
    $this->_urlEncodeTraverse($parts, $data);
    return implode('&', $parts);
  } //function
  
  private function _urlEncodeTraverse(&$parts, $data_portion, $parent = '') {
    foreach ($data_portion as $key => $value) {
      $name = empty($parent) ? urlencode($key)
          : ($parent . '['  . (!is_int($key) ? urlencode($key) : '') .  ']');

      if (is_array($value)) {
        $this->_urlEncodeTraverse($parts, $value, $name);
      } //if
      elseif (is_string($value)) {
        $parts[] = $name . '=' . urlencode($value);
      } //elseif
    } //foreach
  } //function
} //class
