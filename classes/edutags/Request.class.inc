<?php
// $id:$

/**
 * @file
 * This file contains Edutags' Request class.
 */

namespace learnline\search\classes\edutags;

use learnline\search\classes\SearchRequest;

class Request extends SearchRequest {
  protected $service = Service::IDENTIFIER;
  protected $method = self::METHOD__GET;
  
  protected $parameters = array();
  
  public function setResourceUrls(array $urls) {
    $param_urls = implode(',', array_map('urlencode', $urls));
    
    $uri = str_replace(
        array('%KEY', '%ID'), 
        array(Service::getSetting(Service::SETTING__ACCESS_TOKEN), $param_urls), 
        Service::getSetting(Service::SETTING__DOMAIN) .
            Service::getSetting(Service::SETTING__RESOURCE_PATH));
//watchdog('learnline_search', $uri, null, WATCHDOG_DEBUG);
    return $this->setUrl($uri);
  } //function
} //class
