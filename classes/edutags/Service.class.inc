<?php

// $id:$

/**
 * @file
 * Edutags Service class implementation.
 */

namespace learnline\search\classes\edutags;

use learnline\search\classes\SearchService;
use learnline\search\classes\sodis\Service as Sodis;
use LearnlineSearch;

class Service extends SearchService {
  const IDENTIFIER = 'edutags';
  const CACHE = 'cache_edutags_url';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'learnline_search_settings_edutags_search';
  const SETTING__ACCESS_TOKEN = 'learnline_search_settings_edutags_access_token';
  const SETTING__DOMAIN = 'learnline_search_settings_edutags_domain';
  const SETTING__RESOURCE_PATH = 'learnline_search_settings_edutags_resource_path';
  const SETTING__BOOKMARKLET_PATH = 'learnline_search_settings_edutags_bookmarklet_path';
  const SETTING__MAX_TAG_COUNT = 'learnline_search_settings_edutags_max_tag_count';

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @var array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => null,
      self::SETTING__ACCESS_TOKEN => null,
      self::SETTING__DOMAIN => 'http://www.edutags.de',
      self::SETTING__BOOKMARKLET_PATH => '/sites/all/modules/bookmarklet/bookmarklet_learnline.js',
      self::SETTING__RESOURCE_PATH => '/api/v1/%KEY/get_resource_by_url?urls=%ID', 
     self::SETTING__MAX_TAG_COUNT => 10,
    );
  } //function

  /**
   * Shorthand method for (bulk) requesting Edutags data.
   *
   * @param  array $urls An array of unique resource URLs.
   * @return \learnline\search\classes\http\Response
   */
  public static function getMultipleByUrl($urls) {
    if (!self::getSetting(self::SETTING__ACTIVATED)) {
      throw new \RuntimeException(t('Edutags are currently deactivated.'));
    } //if

    $response = Request::create()->setResourceUrls($urls)->send()->getResponse();
    return $response;
  } //function

} //class
