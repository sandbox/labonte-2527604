<?php

// $id:$

/**
 * @file
 * This file contains Edutags specific implementation of Result class.
 */

namespace learnline\search\classes\edutags;

use learnline\search\classes\SearchResult;
use LearnlineSearch;

class Result extends SearchResult {
  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::IDENTIFIER;

  /**
   * Associative array, defining the theme to use for each render format.
   *
   * @var string[]
   */
  protected $theme = 'learnline_search_result_edutags';

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $format_structure = array(
    // Foreign node ID:
    '#id' => '@/nid',
    '#title' => '@/title',
    '#description' => '@/description',
    '#thumbnail' => '@/screenshot',
    '#resource' => '@/url',
    '#tags' => '@/tags/[]',
    '#comments' => '@/comment/[]',
    '#rating' => '@/voting',
    '#license' => '@/license',
  );

  protected $format_callbacks = array(
    '#tags' => 'self::reformatTags',
    '#comments' => 'self::reformatComments',
    '#rating' => 'self::getRatingClass',
  );

  protected function extract($path) {
    assert('is_array($path)');

    $key = reset($path);
    if (isset($this->raw[$key])) {
      $value = $this->raw[$key];
      while ($key = next($path)) {
        if ($key === '[]') {
          break;
        } //if

        is_array($value) && array_key_exists($key, $value)
            ? $value = &$value[$key]
            : $value = '';
      } //while
    } else {
      $value = null;
    } //if

    if (is_array($value) && $key !== '[]') {
      $value = reset($value);
    } //if
    elseif (!is_array($value) && $key === '[]') {
      $value = empty($value) ? array() : array($value);
    } //elseif

    if (!isset($value)) {
      $value = $key === '[]' ? array() : '';
    } //if

    return $value;
  } //function

  public function format($raw = null) {
    if (!isset($raw)) {
      $raw = $this->raw;
    } //if

    // Extract data defined via $this->format_structure.
    foreach ($this->format_structure as $render_key => $data_path) {
      if (is_string($data_path) && strpos($data_path, '@') === 0) {
        $path = explode('/', $data_path);
        array_shift($path);
        $formatted[$render_key] = isset($this->format_callbacks[$render_key]) &&
            is_callable($this->format_callbacks[$render_key])
            ? call_user_func($this->format_callbacks[$render_key],
                $this->extract($path))
            : $this->extract($path);
      } //if
    } //foreach
    
    $formatted['#rating_class'] = 'rating_' . 
        (int) ($formatted['#rating'] / 20) . 'stars';

    return parent::format($formatted);
  } //function

  /**
   * Returns the resource location/URI as string.
   *
   * @return type
   */
  public function getResourceLocation() {
    throw new \Exception('NOT YET IMPLEMENTED!');
  } //function

  /**
   * Re-formats tags.
   * 
   * Input: array( array( ID1, TAG1, COUNT1 ), array( ID2, TAG2, COUNT2 ), ... )
   * Output: array( TAG1, TAG2, ... )
   *
   * @param  array $tags
   * @return array
   */
  public static function reformatTags($tags) {
    uksort($tags, function ($a, $b) { return $a['count'] - $b['count']; });
    
    array_walk($tags, function (&$tag) {
      $tag = array(
        '#tag' => $tag['tag'],
        '#count' => $tag['count'],
      );
    });
    
    return $tags;
  } //function
  
  /**
   * Re-formats comments.
   * 
   * @param  array $comments
   * @return array
   */
  public static function reformatComments($comments) {
    array_walk($comments, function (&$c) {
      $c = array(
        '#username' => $c['user_name'],
        '#title' => $c['subject'],
        '#content' => $c['comment'],
      );
    });
    
    return $comments;
  } //function

  /**
   * Returns the service identifier.
   *
   * @return string
   */
  public function getServiceId() {
    return isset($this->raw['nid']) ? $this->raw['nid'] : FALSE;
  } //fucntion
} //class
