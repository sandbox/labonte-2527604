<?php
// $id:$

/**
 * @file
 * This file contains the Edutags specific implementation of the \SearchResponse
 * interface.
 */

namespace learnline\search\classes\edutags;

use learnline\search\classes\SearchResponse;
use LearnlineSearch;

class Response extends SearchResponse {
  protected $service = Service::IDENTIFIER;
  protected $theme = 'learnline_search_result_edutags_';

  protected function parseData() {
    assert('isset($this->raw->data)');
    $this->data = json_decode($this->raw->data, TRUE);
    $this->results = $this->extractResults();
  } //function

  protected function extractResults() {
    if (!isset($this->data) || !is_array($this->data)) {
      throw new \RuntimeException(
          t('Got strange data portion for an Edutags response.'));
    } //if

    return $this->data;
  } //function

  public function format($raw = null) {
    // Force results to format themselves.
    $this->getResults();
    
    if ($e = $this->getError()) {
      drupal_set_message($e, 'error');
    } //if
  } //fucntion
  
  public function getFormatted() {
    if (empty($this->results)) {
      return array();
    } //if
    
    $result = reset($this->results);
    return $result->getFormatted();
  } //function
  
  public function getOutput() {
    if (empty($this->results)) {
      return '';
    } //if

    return array_map(
        function ($r) { 
          return array(
            'id' => $r['#id'],
            'resource' => $r['#resource'],
            'html' => $r->getOutput(),
          );
        }, 
        $this->results);
  } //function
} //interface
