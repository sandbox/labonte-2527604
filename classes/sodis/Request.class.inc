<?php
// $id:$

/**
 * @file
 * This file contains the Request class interface.
 */

namespace learnline\search\classes\sodis;

use learnline\search\classes\SearchRequest;
use \LearnlineSearch;

class Request extends SearchRequest {
  protected $service = LearnlineSearch::SODIS;
  protected $method = self::METHOD__GET;

  /**
   * Stores parameters in case of calling $this->setParameters().
   *
   * @var array
   */
  protected $parameters = array();

  /**
   * Simply assign the query parameters to get a ready-to-send configured
   * Request object.
   *
   * @param  array $parameters
   * @return \learnline\search\classes\sodis\Request
   */
  public function setParameters($parameters) {
    $this->parameters = $parameters;
    // Determine the 'size' and 'from' parameters:
    $page_multiplier = isset($parameters[LearnlineSearch::PARAM__PAGE])
        ? abs($parameters[LearnlineSearch::PARAM__PAGE] -1) : 0;
    $parameters[LearnlineSearch::PARAM__SIZE] =
        (int) LearnlineSearch::getSetting(LearnlineSearch::SETTING__RESULTS_PER_PAGE);
    $parameters[LearnlineSearch::PARAM__FROM] =
        $page_multiplier * $parameters[LearnlineSearch::PARAM__SIZE];
    // Format query string:
    $query_parameters = substr($this->mapParameters($parameters), 1);
    // Finally format the whole query:
    $base_url = str_replace('%KEY',
        Service::getSetting(Service::SETTING__ACCESS_TOKEN),
        Service::getSetting(Service::SETTING__SEARCH_URI));

    return $this->setUrl($base_url .
        (substr($base_url, -1) === '/' ? '?' : '/?') . $query_parameters);
  } //function

  /**
   * Performs SODIS specific parameter mapping and returns the result as
   * URL-encoded string.
   *
   * @param  array $parameters Array in Drupal QueryArray format.
   * @return string
   */
  public function mapParameters($parameters) {
    $url = '';
    foreach (LearnlineSearch::getInstance()->getParameterMapping() as $local => $map) {
      // Skip invalid and empty parameters:
      if (!isset($parameters[$local])
      || (!is_array($parameters[$local]) && strlen($parameters[$local]) < 1)
      || !(isset($map[LearnlineSearch::SODIS])
        && (is_bool($map[LearnlineSearch::SODIS])
          || (isset($map[LearnlineSearch::SODIS][LearnlineSearch::PARAM])
            && is_string($map[LearnlineSearch::SODIS][LearnlineSearch::PARAM]))))) {
        continue;
      } //if
      
      if ($local === LearnlineSearch::PARAM__SEARCH) {
        $url .= '&' . $map[LearnlineSearch::SODIS][LearnlineSearch::PARAM] . '=' .
            urlencode(preg_replace('/([:])/i', '\\\$1', $parameters[LearnlineSearch::PARAM__SEARCH]));
        continue;
      }

      $url .= $this->queryComponentToString(
          is_bool($map[LearnlineSearch::SODIS])
          ? $local
          : $map[LearnlineSearch::SODIS][LearnlineSearch::PARAM],
          $parameters[$local],
          $map);
    } //foreach
    return $url;
  } //function

  /**
   * Formats an URL component string
   *
   * Examples:
   *      &order=asc
   *  or  &keywords[]=foo&keywords[]=bar
   *
   * @param string $parameter
   * @param string|array $value
   */
  public function queryComponentToString($parameter, $value, $map) {
    $url = '';
    if (is_array($value)) {
      foreach ($value as $val) {
        if (!empty($val)) {
          $url .= '&' . $parameter . '[]=' . urlencode($val);
        } //if
      } //foreach
    } //if
    else {
      if (isset($map[LearnlineSearch::IS_ARRAY]) && $map[LearnlineSearch::IS_ARRAY]) {
        $url .= '&' . $parameter . '[]=' . urlencode($value);
      } //if
      else {
        $url .= '&' . $parameter . '=' . urlencode($value);
      } //else
    } //else
    return $url;
  } //function

  /**
   * Returns parameters after usage of $this->setParameters(); otherwise an
   * empty array will be returned.
   *
   * @return array
   */
  public function getParameters() {
    return $this->parameters;
  } //function
} //class
