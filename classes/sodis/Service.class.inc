<?php

// $id:$

/**
 * @file
 * SodisSearchService implementation.
 */

namespace learnline\search\classes\sodis;

use learnline\search\classes\SearchService;
use LearnlineSearch;

class Service extends SearchService {
  const IDENTIFIER = 'sodis';
  const CACHE = 'cache_learnline_sodis';
  const CACHE_TYPEAHEAD = 'cache_learnline_sodis_typeahead';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'learnline_search_settings_sodis_search';
  const SETTING__ACCESS_TOKEN = 'learnline_search_settings_sodis_access_token';
  const SETTING__TYPEAHEAD_COUNT = 'learnline_search_settings_sodis_typeahead_count';
  const SETTING__TYPEAHEAD_URI = 'learnline_search_settings_sodis_typeahead_uri';
  const SETTING__SEARCH_URI = 'learnline_search_settings_sodis_search_uri';
  const SETTING__RESOURCE_URI = 'learnline_search_settings_sodis_resource_uri';
  const SETTING__RESOURCE_DOWNLOAD_URI = 'learnline_search_settings_sodis_resource_download_uri';
  const SETTING__FACETS_URI = 'learnline_search_settings_sodis_facets_uri';
  const SETTING__THUMBS_DIR = 'learnline_search_settings_sodis_thumbs_dir';
  const SETTING__GET_LATEST_SORTING = 'learnline_search_settings_sodis_get_latest_sorting';
  const SETTING__GET_LATEST_COUNT = 'learnline_search_settings_sodis_get_latest_count';
  const SETTING__GET_LATEST_RESTRICTION = 'learnline_search_settings_sodis_get_latest_restriction';

  const FACETS = '#facets';
  const FACET_MISSING = 'missing';
  const FACET_TOTAL = 'total';
  const FACET_OTHER = 'other';
  const FACET_TERMS = 'terms';
  const FACET_COUNT = 'count';
  const FACET_TERM = 'term';

  const FACET__COMPETENCY = 'competency';
  const FACET__CONTEXT = 'context';
  const FACET__COPYRIGHT = 'copyright';
  const FACET__DISCIPLINE = 'discipline';
  const FACET__KEYWORD = 'keyword';
  const FACET__LANGUAGE = 'language';
  const FACET__RESOURCE_TYPE = 'learningResourceType';
  const FACET__PROVIDER = 'provider';
  const FACET__PUBLISHER = 'publisher';
  
  const ORDER__ASC = 'asc';
  const ORDER__DESC = 'desc';

  /**
   * Array of possible sorting criteria.
   * 
   * @return array
   */
  public static function getSortings() {
    return array(
      'lom:header_datestamp' => t('resource header date'),
      'lom:publisherDate' => t('publisher date'),
      'lom:creatorDate' => t('creator date'),
      'lom:providerDate' => t('provider date'),
      'importDate' => t('import date'),
    );
  } //function

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @var array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => null,
      self::SETTING__ACCESS_TOKEN => null,
      self::SETTING__SEARCH_URI => null,
      self::SETTING__TYPEAHEAD_URI => null,
      self::SETTING__TYPEAHEAD_COUNT => 10,
      self::SETTING__RESOURCE_URI => null,
      self::SETTING__RESOURCE_DOWNLOAD_URI => null,
      self::SETTING__FACETS_URI => null,
      self::SETTING__THUMBS_DIR => '/sites/default/files/images/thumbs',
      self::SETTING__GET_LATEST_COUNT => 10,
      self::SETTING__GET_LATEST_SORTING => '',
      self::SETTING__GET_LATEST_RESTRICTION => 'context[]=Allgemeinbildende+Schule',
    );
  } //function

  /**
   * Shorthand method for requesting SODIS.
   *
   * @param  array $parameters
   * @return \learnline\search\classes\http\Response
   */
  public static function search($parameters) {
    $request = Request::create()->setParameters($parameters);
    $cid = md5($request->getUrl());

//    if ($cached = cache_get($cid, self::CACHE)) {
//      return $cached->data;
//    } //if

    $request->send();
//    cache_set($cid, $request, self::CACHE,
//        LearnlineSearch::getCacheExpirationTime());

    return $request;
  } //function

  /**
   * Returns a staged type ahead request.
   * 
   * @param  string $term
   * @return \learnline\search\classes\sodis\Request
   */
  public static function typeAhead($term) {
    if ($cached = cache_get($term, self::CACHE_TYPEAHEAD)) {
      return $cached->data;
    } //if

    $request = TypeAheadRequest::create()->setTerm($term)->send();
    cache_set($term, $request, self::CACHE_TYPEAHEAD,
        LearnlineSearch::getCacheExpirationTime());

    return $request;
  } //function
 
  /**
   * @todo Remove this method!
   */
  public static function getResourceLocation($resource_id) {
    self::redirect(self::getProviderUrl($resource_id));
  } //function
  
  public static function getProviderUrl($resource_id) {
    // Build resource download URL:
    $url = str_replace(
        array('%KEY', '%RESOURCE'),
        array(self::getSetting(self::SETTING__ACCESS_TOKEN), urlencode($resource_id)),
        self::getSetting(self::SETTING__RESOURCE_DOWNLOAD_URI));

    $response = drupal_http_request($url);

    if (!isset($response->headers['location'])) {
      throw new \RuntimeException(
          t('The SODIS resource you are requesting cannot be retrieved.'), null,
          isset($response->error)
              ? new \Exception($response->error, $response->code)
              : null);
    } //if
    
    return $response->headers['location'];
  } //function

  /**
   * Performs a redirect to the given SODIS resource by sending the appropriate 
   * HTTP location header.
   * 
   * @param  string $resource_id
   * @throws \RuntimeException
   */
  public static function redirect($location) {
    // Send HTTP Location header for a redirect and stop execution:
    drupal_send_headers(array('Location' => $location));
  } //function
  
  /**
   * Returns a Request class containing the "neueste Materialien".
   * 
   * @param  int $num
   * @return \learnline\search\classes\sodis\Request
   */
  public static function getLatest($num = null) {
    $num = isset($num) && ($intNum = (int) $num) > 0 ? $intNum
      : self::getSetting(self::SETTING__GET_LATEST_COUNT);
    
    $url = str_replace('%KEY', self::getSetting(self::SETTING__ACCESS_TOKEN),
        self::getSetting(self::SETTING__SEARCH_URI));
    
    $sortings = self::getSortings();
    $sorting = self::getSetting(self::SETTING__GET_LATEST_SORTING);
    
    $url .= '?' . self::getSetting(self::SETTING__GET_LATEST_RESTRICTION) .
        (isset($sortings[$sorting]) ? '&sort=' . urlencode($sorting) : '') .
        '&from=0' . '&size=' . $num . 
        '&order=' . self::ORDER__DESC;
    
    return Request::create()->setUrl($url)->send();
  } //function
  
  /**
   * Escapes resource IDs by escaping each ':' with a '\' (backslash).
   * 
   * @param  string $resource_id
   * @return string
   */
  public static function escapeId($resource_id) {
    return urlencode(
        preg_replace(
            array('/([^\\\]):/', '/([^\\\])\//'), 
            array('$1\:', '$1\\/'), 
            $resource_id));
  } //function
} //class
