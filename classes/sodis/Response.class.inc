<?php
// $id:$

/**
 * @file
 * This file contains the SODIS specific implementation of the \SearchResponse
 * interface.
 */

namespace learnline\search\classes\sodis;

use learnline\search\classes\SearchResponse;
use LearnlineSearch;

use learnline\search\classes\edmond;

class Response extends SearchResponse {
  const CALLBACK = 'callback';
  
  protected $service = LearnlineSearch::SODIS;
  protected $theme = 'learnline_search_results';
  
  /**
   * Renderable formatted sidebar.
   *
   * @var array
   */
  protected $sidebar = array();

  protected $facets = array();

  protected $facet_mapping = array(
    Service::FACET__COMPETENCY => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__COMPETENCIES,
    ),
    Service::FACET__CONTEXT => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__CONTEXTS,
    ),
    Service::FACET__DISCIPLINE => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__DISCIPLINES,
    ),
    Service::FACET__KEYWORD => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__KEYWORDS,
    ),
    Service::FACET__LANGUAGE => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__LANGUAGES,
      self::CALLBACK => '\learnline\search\classes\sodis\Response::langMapping',
    ),
    Service::FACET__PROVIDER => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__PROVIDERS,
    ),
    Service::FACET__PUBLISHER => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__PUBLISHERS,
    ),
    Service::FACET__COPYRIGHT => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__COPYRIGHTS,
//      self::CALLBACK => '\learnline\search\classes\sodis\Result::ccFilter',
    ),
    Service::FACET__RESOURCE_TYPE => array(
      LearnlineSearch::PARAM => LearnlineSearch::PARAM__CONTENTTYPES,
    ),
  );
  
  protected static $lang_mapping = array(
    'deutsch' => array('de', 'Deutsch'),
    'englisch' => array('en', 'Englisch'),
    'französisch' => array('fr', 'Französisch'),
    'spanisch' => array('es', 'Spanisch'),
    'türkisch' => array('tr', 'tk', 'Türkisch'),
    'russisch' => array('ru', 'Russisch'),
    'arabisch' => array('ar', 'Arabisch'),
  );

  protected function parseData() {
    assert('isset($this->raw->data)');
    $this->data = json_decode($this->raw->data, TRUE);
  } //function

  protected function extractResults() {
    if (!isset($this->data['hits']) || !isset($this->data['hits']['hits']) ||
        !is_array($this->data['hits']['hits'])) {
      throw new \RuntimeException(
          t('Got strange data portion for a SODIS response.'));
    } //if

    return $this->filterEdmond($this->data['hits']['hits']);
  } //function

  public function format($raw = null) {
    $parameters = drupal_get_query_parameters(NULL, array('q'));
    
    if (isset($parameters[LearnlineSearch::PARAM__PAGE])) {
      $page = (int) $parameters[LearnlineSearch::PARAM__PAGE];
      unset($parameters[LearnlineSearch::PARAM__PAGE]);
    } //if
    else {
      $page = 1;
    } //else

    $formatted = array(
      '#children' => $this->getResults(),
      '#searchword' => isset($parameters[LearnlineSearch::PARAM__SEARCH])
          ? $parameters[LearnlineSearch::PARAM__SEARCH] : '',
      '#total' => (int) $this->data['hits']['total'],
      '#per_page' => (int) LearnlineSearch::getSetting(LearnlineSearch::SETTING__RESULTS_PER_PAGE),
      '#page_no' => $page,
      '#pagination_base' => '?' . $this->urlEncode($parameters) . '&page=',
      '#error' => $this->getError() ?: null,
      '#bottom' => print_r($this->pocStats, true),
    );

    $this->formatSidebar();
    return parent::format($formatted);
  }

  public function getFormattedSidebar() {
    // Add inline Javascript to the footer:
    $js_inline_opts = array(
      'type' => 'inline',
      'scope' => 'footer',
    );
    drupal_add_js("new learnline.PageOverlay('#rechtshinweis_show', '#rechtshinweis_text');",
        $js_inline_opts);
    
    return $this->sidebar;
  }
  
  public function formatSidebar() {
    assert('isset($this->data)');
    assert('is_array($this->data)');
    $parameters = drupal_get_query_parameters();

    $legal_notice = LearnlineSearch::getSetting(LearnlineSearch::SETTING__LEGAL_NOTICE);
    $legal_notice['heading'] = LearnlineSearch::getSetting(LearnlineSearch::SETTING__LEGAL_NOTICE_HEADING);
    
    $this->sidebar = array(
      '#theme' => 'learnline_search_results_sidebar',
      '#legal_notice' => $legal_notice,
      '#query_url' => '?' . $this->urlEncode($parameters),
      '#facet_base_url' => '?' . $this->urlEncode(drupal_get_query_parameters(
          $parameters, array(LearnlineSearch::PARAM__PAGE))),
    );

    if (!isset($this->sidebar[Service::FACETS])) {
      // Iterate all facets registered to the Service class.
      foreach (LearnlineSearch::getInstance()->getParameterMapping() as $param => $mapping) {
        // Exclude all unwanted facets.
        if ((array_key_exists(LearnlineSearch::HIDDEN, $mapping) && 
            $mapping[LearnlineSearch::HIDDEN]) ||
            !isset($mapping[LearnlineSearch::TITLE]) ||
            !isset($mapping[LearnlineSearch::SODIS])) {
          continue;
        } //if

        // Create initial array to hold the facet value definitions for the sidebar.
        $this->sidebar[Service::FACETS][$param] = array();
      } //foreach

      // Now let's parse the SODIS response to extract valid facets.
      foreach ($this->data['facets'] as $facet_key => $facet) {
        // Ignore facets with less than one result match. (Those shouldn't even exist!)
        if ($facet[Service::FACET_TOTAL] < 1 ||
            !($facet_name = $this->mapFacetKey($facet_key))) {
          continue;
        } //if

        // Also skip the ones not registered to the Service class.
        if (!array_key_exists($facet_name, $this->sidebar[Service::FACETS])) {
          continue;
        } //if
        
        // Iterate possible values of the current facet.
        foreach ($facet[Service::FACET_TERMS] as $facet_value) {
          // Add facet value definition containing the display name and the result
          // match count for that facet.
          $this->sidebar[Service::FACETS][$facet_name][$facet_value[Service::FACET_TERM]] = array(
            LearnlineSearch::DISPLAY_VALUE => html_entity_decode($facet_value[Service::FACET_TERM]),
            LearnlineSearch::COUNT => $facet_value[Service::FACET_COUNT],
          );

          // Check if the current facet value has already been set as request parameter.
          if (isset($parameters[$facet_name]) &&
              is_string($parameters[$facet_name]) &&
              $parameters[$facet_name] === $facet_value[Service::FACET_TERM]) {
            // Get query parameters as associative array, excluding the current
            // facet value.
            $query = drupal_get_query_parameters($parameters, array($facet_name, LearnlineSearch::PARAM__PAGE));
            // Create a query string for unsetting this facet value.
            $this->sidebar[Service::FACETS][$facet_name][$facet_value[Service::FACET_TERM]][LearnlineSearch::ACTIVE] =
                LearnlineSearch::getInstance()->queryToString($query);
          } //if
          // Check if the current facet value has already been set as an array in request parameters.
          elseif (isset($parameters[$facet_name]) &&
              is_array($parameters[$facet_name]) &&
              is_numeric($idx = array_search($facet_value[Service::FACET_TERM], $parameters[$facet_name]))) {
            // Get query parameters as associative array, excluding the current
            // facet value.
            $query = drupal_get_query_parameters($parameters, array("{$facet_name}[{$idx}]", LearnlineSearch::PARAM__PAGE));
            // Create a query string for unsetting this facet value.
            $this->sidebar[Service::FACETS][$facet_name][$facet_value[Service::FACET_TERM]][LearnlineSearch::ACTIVE] =
                LearnlineSearch::getInstance()->queryToString($query);
          } //if
        } //foreach
      } //foreach
      
      // Ugly workaround for language mapping:
      if (isset($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES]) &&
          is_array($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES])) {
        foreach ($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES] as $lang_key => $lang) {
          if ($language = $this->mapLanguage($lang_key)) {
            $correlated = array(
              LearnlineSearch::DISPLAY_VALUE => $language,
              LearnlineSearch::COUNT => $lang[LearnlineSearch::COUNT],
            );
            
            if (isset($lang[LearnlineSearch::ACTIVE])) {
              $correlated[LearnlineSearch::ACTIVE] = '?' . $this->urlEncode(
                  $this->queryRemoveLanguage($language));
            } //if
            else {
              $correlated[LearnlineSearch::HREF] = '?' . $this->urlEncode(
                  $this->queryAddLanguage($language));
            } //else

            unset($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES][$lang_key]);
            
            foreach (self::$lang_mapping[$language] as $lang_short) {
              if ($lang_short === $language) {
                continue;
              } //if
              if (isset($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES][$lang_short])) {
                $correlated[LearnlineSearch::COUNT] += $this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES][$lang_short][LearnlineSearch::COUNT];
                unset($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES][$lang_short]);
              } //if
            } //foreach
            
            $this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES][$language] = $correlated;
          } //if
        } //foreach
        
        uasort($this->sidebar[Service::FACETS][LearnlineSearch::PARAM__LANGUAGES], array($this, '_facetSort'));
      } //if
    } //if

    return $this->sidebar;
  } //function
  
  private function _facetSort($a, $b) {
    if (isset($a[LearnlineSearch::ACTIVE]) && !isset($b[LearnlineSearch::ACTIVE])) {
      return -1;
    } //if
    elseif (!isset($a[LearnlineSearch::ACTIVE]) && isset($b[LearnlineSearch::ACTIVE])) {
      return 1;
    } //elseif
    elseif ($a[LearnlineSearch::COUNT] !== $b[LearnlineSearch::COUNT]) {
      return $b[LearnlineSearch::COUNT] - $a[LearnlineSearch::COUNT];
    } //elseif
    else {
      return strnatcmp($b[LearnlineSearch::DISPLAY_VALUE], 
          $a[LearnlineSearch::DISPLAY_VALUE]);
    } //else
  } //function

  /**
   * Maps facet keys on corresponding query parameters.
   *
   * @param  string $key
   * @return string|FALSE
   */
  public function mapFacetKey($key) {
    $value = isset($this->facet_mapping[$key]) &&
        isset($this->facet_mapping[$key][LearnlineSearch::PARAM])
        ? $this->facet_mapping[$key][LearnlineSearch::PARAM]
        : FALSE;
    
    return $value;
  } //function
  
  /**
   * Reverts the language mapping array, to simplify working with it.
   * 
   * @return array
   */
  public function mapLanguage($lang) {
    static $mapper;
    
    if (!isset($mapper)) {
      foreach (self::$lang_mapping as $display_name => $matches) {
        foreach ($matches as $match) {
          $mapper[$match] = $display_name;
        } //foreach
      } //foreach
    } //if
    
    $lang = strtolower($lang);    
    return isset($mapper[$lang]) ? $mapper[$lang] : FALSE;
  } //function
  
  /**
   * Adds a language to the given or current query parameters.
   * 
   * @param  string $lang
   * @param  array  $query_parameters
   * @return array
   */
  public function queryAddLanguage($lang, $query_parameters = NULL) {
    if (!isset($query_parameters)) {
      $query_parameters = drupal_get_query_parameters(NULL, array('q', 
          LearnlineSearch::PARAM__PAGE));
    } //if

    $query_parameters[LearnlineSearch::PARAM__LANGUAGES] = array_merge(
        isset($query_parameters[LearnlineSearch::PARAM__LANGUAGES])
            ? $query_parameters[LearnlineSearch::PARAM__LANGUAGES]
            : array(),
        isset(self::$lang_mapping[$lang])
            ? array_merge(self::$lang_mapping[$lang], array($lang))
            : array($lang));

    return $query_parameters;
  } //function
  
  /**
   * Removes the a language from given or current query parameter array.
   * 
   * @param  string $lang
   * @param  array  $query_parameters
   * @return array
   */
  public function queryRemoveLanguage($lang, $query_parameters = NULL) {
    if (!isset($query_parameters)) {
      $query_parameters = drupal_get_query_parameters(NULL, array('q', 
          LearnlineSearch::PARAM__PAGE));
    } //if
    
    // There is no language parameter to remove. So we can just return.
    if (!isset($query_parameters[LearnlineSearch::PARAM__LANGUAGES])) {
      return $query_parameters;
    } //if
    
    if (isset(self::$lang_mapping[$lang])) {
      if (is_int($idx = array_search($lang, $query_parameters[LearnlineSearch::PARAM__LANGUAGES]))) {
        unset($query_parameters[LearnlineSearch::PARAM__LANGUAGES][$idx]);
      } //if
      foreach (self::$lang_mapping[$lang] as $lang_short) {
        if (is_int($idx = array_search($lang_short, $query_parameters[LearnlineSearch::PARAM__LANGUAGES]))) {
          unset($query_parameters[LearnlineSearch::PARAM__LANGUAGES][$idx]);
        } //if
      } //foreach
    } //if
    elseif (is_int($idx = array_search($lang, $query_parameters[LearnlineSearch::PARAM__LANGUAGES]))) {
      unset($query_parameters[LearnlineSearch::PARAM__LANGUAGES][$idx]);
    } //else
    
    return $query_parameters;
  } //function

  /**
   * @param array $resultset Numerical array containing the results.
   * @return array
   */
  public function filterEdmond($resultset) {
    /**
     * @var $edmondIds
     * Keys: EDMOND ID
     * Values: Index for the result in $resultset
     */
    $edmondIds = array();
    $this->pocStats['ids'] = array();
    $this->pocStats['idsMatched'] = array();
    foreach ($resultset as $resultKey => $result) {
      $this->pocStats['ids'][$resultKey] = $result['_id'];
      $hit = array();
      if (preg_match('/^(FWU-0|BWS-0)(.+)$/i', $result['_id'], $hit)) {
        $this->pocStats['idsMatched'][] = $hit;
        $edmondIds[$hit[2]] = $resultKey;
      }
    }
    $edmonedTotalCnt = count($edmondIds);

    if ($edmonedTotalCnt > 0) {
      $edmondRequest = edmond\Request::create()
      ->setParametersForLicenseRequest(drupal_get_query_parameters(), array_flip($edmondIds));
      $this->pocStats['request'] = array(
        'url' => $edmondRequest->getUrl(),
        'postData' => $edmondRequest->getRawData(),
      );
      $edmondResponse = $edmondRequest->send()->getResponse()->getData();
      $this->pocStats['response'] = $edmondResponse->asXML();
      if (isset($edmondResponse) && $edmondResponse instanceof \SimpleXMLElement) {
        if (isset($edmondResponse->r)) {
          $edmondLicensedCnt = 0;
          $edmondResults = array();
          $this->pocStats['licensed'] = array();
          $this->pocStats['unknownEdmondIdFormat'] = array();
          foreach ($edmondResponse->r as $result) {
            $attr = $result->attributes();
            $edmondId = (string)$attr['identifier'];
            $edmondRawId = array();
            if (preg_match('/^[^-]+-(.+)$/i', $edmondId, $edmondRawId)) {
              $resultKey = $edmondIds[$edmondRawId[1]];
              $edmondResults[] = $resultKey;
              $edmondLicensedCnt++;
              $this->pocStats['licensed'][] = $edmondRawId;
            } else {
              $this->pocStats['unknownEdmondIdFormat'][] = $edmondId;
            }
          }
          // Remove all EDMOND results without a license
          $this->pocStats['removedIds'] = array();
          foreach (array_diff($edmondIds, $edmondResults) as $noLicenseResultId) {
            unset($resultset[$noLicenseResultId]);
            $this->pocStats['removedIds'][] = $noLicenseResultId;
          }

          drupal_set_message(t('Removed %missingLicense of %total EDMOND items.', array(
            '%missingLicense' => $edmonedTotalCnt - $edmondLicensedCnt,
            '%total' => $edmonedTotalCnt,
          )));
        } else {
          $this->pocStats['removedIds'] = array();
          foreach ($edmondIds as $resultKey) {
            unset($resultset[$resultKey]);
            $this->pocStats['removedIds'][] = $resultKey;
          }
          drupal_set_message(t('Got an empty result from EDMOND, so we removed all EDMOND related items from the search results.'));
        }
      } else {
        drupal_set_message(t('Failed requesting EDMOND.'), 'error');
      }
    } else {
      drupal_set_message(t('Did not find any EDMOND match.'));
    }

    return $resultset;
  }

  public $pocStats = array();
} //interface
