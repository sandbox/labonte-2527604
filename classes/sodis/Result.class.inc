<?php

// $id:$

/**
 * @file
 * This file extends the SearchResultBase class to a SODIS specific form.
 */

namespace learnline\search\classes\sodis;

use learnline\search\classes\SearchResult;
use LearnlineSearch;

class Result extends SearchResult {
  /**
   * Parameter: Resource URI.
   */
  const RESOURCE = 'resource';

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = LearnlineSearch::SODIS;

  /**
   * Associative array, defining the theme to use for each render format.
   *
   * @var string[]
   */
  protected $theme = 'learnline_search_result';

  /**
   * Associative array representing the structure of the formatted array.
   *
   * @var array
   */
  protected $format_structure = array(
    '#id' => '@/_id',
    '#title' => '@/_source/lom:general_title',
    '#description' => '@/_source/lom:general_description',
    '#thumbnail' => '@/_source/lom:relation_hasThumbnail',
    '#publisher' => '@/_source/lom:publisher/[]',
    '#provider' => '@/_source/lom:provider',
    '#contenttype' => '@/_source/lom:technical_format',
    '#resourcetype' => '@/_source/ll3:learningResourceType/[]',
    '#cc' => '@/_source/lom:rights',
    '#usage' => '@/_source/lom:rights_de',
    '#age' => '@/_source/lom:educational_typicalAgeRange/[]',
    '#subject' => '@/_source/lom:classificationDisciplineEntry/[]',
    '#language' => '@/_source/lom:general_language',
    '#origin' => '@/_source/lom:technical_location',
  );

  protected $format_callbacks = array(
    '#thumbnail' => 'self::prepareThumb',
    '#cc' => 'self::ccFilter',
    '#contenttype' => 'self::mimeSubtype',
    '#resourcetype' => 'self::resourceTypeFilter',
  );

  /**
   * Mapping array for resource type based default thumbnails.
   *
   * @var array
   */
  protected $default_thumbnails = array(
    '' => 'default.png',
    'arbeitsblatt' => 'arbeitsblatt.jpg',
    'audio' => 'audio.jpg',
    'bild' => 'bild.jpg',
    'film' => 'film.jpg',
    'interaktives material' => 'interaktiv.jpg',
    'weiteres' => 'weiteres.jpg',
    'werkzeug' => 'werkzeug.jpg',
  );

  protected function extract($path) {
    assert('is_array($path)');

    $key = reset($path);
    if (isset($this->raw[$key])) {
      $value = $this->raw[$key];
      while ($key = next($path)) {
        if ($key === '[]') {
          break;
        } //if

        is_array($value) && array_key_exists($key, $value)
            ? $value = &$value[$key]
            : $value = '';
      } //while
    } //if

    if (is_array($value) && $key !== '[]') {
      $value = reset($value);
    } //if
    elseif (!is_array($value) && $key === '[]') {
      $value = empty($value) ? array() : array($value);
    } //elseif

    if (!isset($value)) {
      $value = $key === '[]' ? array() : '';
    } //if

    return $value;
  } //function

  public function format($raw = null) {
    $formatted = array('#theme' => $this->theme);

    if (!isset($raw)) {
      $raw = $this->raw;
    } //if

    // Extract data defined via $this->format_structure.
    foreach ($this->format_structure as $render_key => $data_path) {
      if (is_string($data_path) && strpos($data_path, '@') === 0) {
        $path = explode('/', $data_path);
        array_shift($path);
        if (isset($this->format_callbacks[$render_key]) && is_callable($this->format_callbacks[$render_key])) {
          $data = $this->extract($path);
          $formatted[$render_key] = call_user_func($this->format_callbacks[$render_key], $data);
          $formatted[$render_key.'_backup'] = $data;
        } else {
          $formatted[$render_key] = $this->extract($path);
        }
      } //if
    } //foreach

    // Add direct link to learning resource.
    $formatted['#href'] = '/sodis/resource/' .
        urlencode($formatted['#id']);

    // Add link for displaying resource's meta data on SODIS.
    $formatted['#metalink'] = 'https://sodis.de/cp/ll.php?idnr=' .
        urlencode($formatted['#id']);

    // Add link to stage a search request for the given resource ID.
    $formatted['#singlesearch'] = '/learnline/search?exposed=1&search=' .
        Service::escapeId($formatted['#id']);

    // If missing, add a default thumbnail image.
    if (!isset($formatted['#thumbnail']['#src']) ||
        empty($formatted['#thumbnail']['#src'])) {
      $thumbnail_path = Service::getSetting(Service::SETTING__THUMBS_DIR);

      if (isset($this->default_thumbnails[$formatted['#resourcetype']])) {
        $formatted['#thumbnail']['#src'] = $thumbnail_path . '/' .
            $this->default_thumbnails[$formatted['#resourcetype']];
      } //if
      else {
        $formatted['#thumbnail']['#src'] = $thumbnail_path . '/' .
            $this->default_thumbnails[''];
      } //else
    } //if

    $formatted['#rating'] = array();

    return $formatted;
  } //function

  /**
   * Returns the resource location/URI as string.
   *
   * @return type
   */
  public function getResourceLocation() {
    throw new \Exception('NOT YET IMPLEMENTED!');
  } //function

  /**
   * Wraps the given thumbnail source string into a renderable array.
   *
   * @param  string $thumbnail
   * @return array
   */
  public static function prepareThumb($thumbnail) {
    return array(
      '#theme' => 'learnline_search_result_thumbnail',
      '#src' => $thumbnail,
    );
  } //function

  /**
   * Returns filtered copyright values.
   *
   * @param  string $input String to be filtered.
   * @return string        Returns the filtered string.
   */
  public static function ccFilter($input) {
    $map = array(
      'by-nc-nd' => 'http://creativecommons.org/licenses/by-nc-nd/3.0/de/',
      'by-nc' => 'http://creativecommons.org/licenses/by-nc/3.0/de/',
      'by-nc-sa' => 'http://creativecommons.org/licenses/by-nc-sa/3.0/de/',
      'by-nd' => 'http://creativecommons.org/licenses/by-nd/3.0/de/',
      'by' => 'http://creativecommons.org/licenses/by/3.0/de/',
      'by-sa' => 'http://creativecommons.org/licenses/by-sa/3.0/de/',
      'cc-zero' => 'http://creativecommons.org/about/cc0',
      'publicdomain' => 'http://creativecommons.org/about/pdm',
    );
    $match = array();
    $output = array();

    if (is_string($input)) {
      if (!preg_match('/^cc[:\-](.+)$/i', $input, $match)) {
        return '';
      } //if

      $output['#raw'] = $input;
      $output['#lc'] = strtolower($input);
      $output['#alt'] = strtoupper($input);
      $output['#href'] = isset($map[$match[1]]) ? $map[$match[1]] : FALSE;
      $output['#src'] = LearnlineSearch::getSetting(
          LearnlineSearch::SETTING__CC_DIR) . strtolower($match[1]) . '.png';
    } //if

    return $output;
  } //function

  /**
   * Returns filtered MIME type value (usually the subtype only).
   *
   * @param  string $mimeType
   * @return string
   */
  public static function mimeSubtype($mimeType) {
    $match = array();
    if (preg_match('/^[^\/]*\/(.+)$/', $mimeType, $match)) {
      return $match[1];
    }
    return $mimeType;
  } //function

  /**
   * Implodes resource type array.
   *
   * @param  array $resourceTypes
   * @return string
   */
  public static function resourceTypeFilter($resourceTypes) {
    return implode(', ', $resourceTypes);
  } //function

  /**
   * Returns the service identifier.
   *
   * @return string
   */
  public function getServiceId() {
    return isset($this->raw['_id']) ? $this->raw['_id'] : FALSE;
  } //fucntion
} //class
