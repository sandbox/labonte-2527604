<?php
// $id:$

/**
 * @file
 * This file contains the TypeAheadRequest class definition.
 */

namespace learnline\search\classes\sodis;

use learnline\search\classes\SearchRequest;
use LearnlineSearch;

class TypeAheadRequest extends SearchRequest {
  protected $service = LearnlineSearch::SODIS;
  protected $method = self::METHOD__GET;

  /**
   * This is where we store the decoded results.
   *
   * @var array
   */
  protected $results = array();

  /**
   * Completely configure the Request object by specifying the search term.
   *
   * @param  string $term
   * @return \learnline\search\classes\sodis\TypeAhead
   */
  public function setTerm($term) {
    return $this->setUrl(str_replace('%KEY',
            Service::getSetting(Service::SETTING__ACCESS_TOKEN),
            Service::getSetting(Service::SETTING__TYPEAHEAD_URI)) .
        '?q=' . urlencode($term) . '&count=' .
        Service::getSetting(Service::SETTING__TYPEAHEAD_COUNT));
  } //function

  public function getResults() {
    return $this->results;
  } //function

  public function getResult($index) {
    if (!array_key_exists($index, $this->results)) {
      return FALSE;
    } //if

    return $this->results[$index];
  } //function

  public function send() {
    // Stage request and check response.
    $this->response = drupal_http_request($this->getUrl(), $this->getOptions());
    if ((int) $this->response->code !== 200) {
      throw new \RuntimeException($this->response->status_message,
          $this->response->code);
    } //if
    elseif (!isset($this->response->data)) {
      throw new \RuntimeException('Received response without data portion.');
    } //if

    // Try decoding response data and check result.
    $data = drupal_json_decode($this->response->data);
    if (!isset($data)) {
      throw new \RuntimeException('Invalid JSON in TypeAhead data portion.');
    } //if

    // Reformat results to fit Drupal's auto-completion conventions.
    $this->results = $this->reformatResults($data);

    return $this;
  } //function

  /**
   * Reformats SODIS TypeAhead response data to fit Drupal's auto-completion
   * conventions.
   *
   * @param  array $data
   * @return array
   */
  protected function reformatResults($data) {
    /**
     * @notice The REST service orders its suggestions descending by their
     *         result count. So they are implicitely sorted by relevance...
     */

    $i = 1;
    $results = array();
    // Iterate response data and
    foreach ($data as $hit) {
      // Reformat data to be compatible with Drupal's built-in autocompletion:
      $term = check_plain($hit['term']);
      $results[$term] = $term;
      // Break the loop if we reach the configured count of suggestions:
      if (++$i > Service::getSetting(Service::SETTING__TYPEAHEAD_COUNT)) {
        break;
      } //if
    } //foreach;

    return $results;
  } //function
} //class
