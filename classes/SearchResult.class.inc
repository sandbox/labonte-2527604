<?php
// $id:$

/**
 * @file
 * This file contains the SearchResultBase class implementing the SearchResult
 * interface.
 */

namespace learnline\search\classes;

use learnline\search\interfaces\SearchResult as SearchResultIntf;
use LearnlineSearch;

abstract class SearchResult extends HtmlView implements SearchResultIntf {
  /**
   * Parameter: Result node ID.
   */
  const ID = '#id';

  /**
   * Parameter: Result type.
   */
  const TYPE = 'type';

  /**
   * Parameter: Request URI.
   */
  const REQUEST_URI = 'uri';

  /**
   * Service identifier.
   *
   * One of
   *  * \learnline\search\classes\LearnlineSearch::SODIS
   *  * \learnline\search\classes\LearnlineSearch::EDMOND
   *
   * @var string
   */
  protected $service;

  /**
   * Request parameter and facet mapping.
   *
   * @var array
   */
  protected $mapping;

  /**
   * Renderable result data.
   *
   * @var array
   */
  protected $data;

  /**
   * Drupal node object.
   *
   * @var \stdClass
   */
  protected $node;

  /**
   * Object instances for cloning/factory implementation.
   *
   * @var \learnline\search\classes\SearchResult[]
   */
  protected static $instance;

  public function __construct() {
    parent::__construct();
    assert('isset($this->service)');
    assert('is_string($this->service)');

    foreach (LearnlineSearch::getInstance()->getParameterMapping() as $param => $mapping) {
      if (!isset($mapping[$this->service])) {
        continue;
      } //if

      $this->mapping[$param] = $mapping;
    } //foreach
  } //function
} //class
