<?php
// $id:$

/**
 * @file
 * This file contains the SearchResultBase class implementing the SearchResult
 * interface.
 */

namespace learnline\search\classes;

use learnline\search\interfaces\SearchResponse as SearchResponseIntf;

abstract class SearchResponse extends HtmlView implements SearchResponseIntf {
  /**
   * Service identifier.
   *
   * One of
   *  * LearnlineSearch::SODIS
   *  * LearnlineSearch::EDMOND
   *
   * @var string
   */
  protected $service;

  /**
   * Originating request.
   *
   * @var \learnline\search\classes\SearchRequest
   */
  protected $request;

  /**
   * Array of results.
   *
   * @var \learnline\search\classes\SearchResult[]
   */
  protected $results = array();

  /**
   * Response data portion as probably Array, ArrayObject or SimpleXMLElement.
   *
   * @var mixed
   */
  protected $data;

  /**
   * Initially formats response data.
   *
   * @return \learnline\search\classes\http\Response Returns itself.
   */
  protected function parseData() {
    throw new \ErrorException(
        t('@class must implement/override method parseData().', array(
            '@class' => get_called_class())));
  } //function

  /**
   * Extracts and returns the search results from formatted data.
   *
   * This method is called after initial formatting through $this->formatData().
   *
   * @return array
   */
  protected function extractResults() {
    throw new \ErrorException(
        t('@class must implement/override method extractResults().', array(
            '@class' => get_called_class())));
  } //function

  /**
   * Contructor.
   */
  public function __construct() {
    parent::__construct();
    assert('isset($this->service)');
    assert('is_string($this->service)');
    assert('isset($this->theme)');
  } //function

  /**
   * Parse raw result data.
   *
   * @param  \learnline\search\classes\SearchRequest $request Orig. request.
   * @return \learnline\search\classes\SearchResponse         Returns itself.
   */
  public function parse($request) {
    $this->request = $request;
    $raw = $request->getRawResponse();
    assert('isset($raw->status_message) || isset($raw->error)');
    assert('isset($raw->request)');
    assert('isset($raw->code)');

    $this->raw = $raw;
    if (!$this->getError()) {
      $this->parseData();

      $factory = '\learnline\search\classes\\' . $this->service .
          '\Result::create';
      $raw_results = $this->extractResults();
      assert('is_array($raw_results)');
      assert('is_callable($factory)');

      $this->results = array_map($factory, $raw_results);
      $this->formatted = $this->format($this->raw);
    } //if

    return $this;
  } //function

  /**
   * Implements Countable.
   *
   * @return int
   */
  public function count() {
    return count($this->results);
  } //function

  /**
   * Implements IteratorAggregate.
   *
   * @return \ArrayIterator
   */
  public function getIterator() {
    return new ArrayIterator($this->results);
  } //function

  /**
   * Implements magic __set() method.
   *
   * @param  string        $name  Result ID.
   * @param  \learnline\search\classes\SearchResult $value Result.
   * @throws RuntimeException
   */
  public function __set($name, $value) {
    if ($value instanceof \SearchResult) {
      $this->results[$name] = $value;
    } //if
    else {
      throw new RuntimeException(
          t('Only \SearchResult implementations can be set as \SearchResponse value.'));
    } //else
  } //function

  /**
   * Implements magic __get() method.
   *
   * @param  string $name     Result ID.
   * @return \learnline\search\classes\SearchResult The search result with ID $name.
   * @throws RuntimeException
   */
  public function __get($name) {
    return $this->getResult($name);
  } //function

  /**
   * Get the originating HTTP request as raw string.
   *
   * @return string|FALSE
   */
  public function getRequestString() {
    return isset($this->raw) && isset($this->raw->request) ? $this->raw->request : FALSE;
  } //function

  /**
   * Get HTTP response code.
   *
   * @return int
   */
  public function getStatusCode() {
    return $this->raw->code;
  } //function

  /**
   * Get the HTTP status message.
   *
   * @return string
   */
  public function getStatusMessage() {
    return isset($this->raw->error)
        ? $this->raw->error
        : $this->raw->status_message;
  } //function

  /**
   * Get HTTP error message or FALSE if no error occurred.
   *
   * @return string|FALSE
   */
  public function getError() {
    return isset($this->raw->error) ? $this->raw->error : FALSE;
  } //function

  /**
   * Get the HTTP version string e.g. 'HTTP/1.1'.
   *
   * @return string|FALSE
   */
  public function getVersion() {
    return isset($this->raw->protocol) ? $this->raw->protocol : FALSE;
  } //function

  /**
   * Get the initial HTTP status code, if the request was redirected.
   *
   * @return int|FALSE
   */
  public function getRedirectStatusCode() {
    return isset($this->raw->redirect_code) ? $this->raw->redirect_code : FALSE;
  } //function

  /**
   * Get the target's URL string in case of a redirect; otherwise FALSE.
   *
   * @return string|FALSE
   */
  public function getRedirectUrl() {
    return isset($this->raw->redirect_url) ? $this->raw->redirect_url : FALSE;
  } //function

  /**
   * Get the HTTP response headers as associative array.
   *
   * @return array|FALSE
   */
  public function getHeaders() {
    return isset($this->raw->headers) ? $this->raw->headers : FALSE;
  } //function

  /**
   * Returns the raw response body.
   *
   * @return string|FALSE
   */
  public function getRawData() {
    return isset($this->raw->data) ? $this->raw->data : FALSE;
  } //function

  /**
   * Returns an appropriate PHP representation for response data.
   *
   * Data format depends on the service and should be in an appropriate PHP
   * representation for the raw response format.
   * (eg. JSON -> Array/ArrayObject, XML -> SimpleXMLElement)
   *
   * @return mixed
   */
  public function getData() {
    return $this->data;
  } //function

  /**
   * Returns an array containing all results.
   * Array keys are the appropriate result IDs.
   *
   * @return \learnline\search\classes\SearchResult[] The actual results.
   */
  public function getResults() {
    return $this->results;
  } //function

  /**
   * Returns the result specified by its index.
   *
   * @param  string $index Result ID.
   * @return \learnline\search\classes\SearchResult The search result with ID $index.
   */
  public function getResult($id) {
    if (array_key_exists($id, $this->results)) {
      return $this->results[$id];
    } //if
    else {
      throw new \RuntimeException(
          t("This \SearchResponse contains no result with ID '@id'.", array(
            '@id' => $id)));
    } //else
  } //function

  public static function getLast() {
    $class = get_called_class();
    if (!isset(self::$instance[$class])) {
      throw new \RuntimeException('No Response received/built yet.');
    } //if

    return self::$instance[$class];
  }
} //interface
