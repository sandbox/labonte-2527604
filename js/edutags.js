// $Id:$

/**
 * @file
 * Javascript implementation of type-ahead functionality.
 */

if (window.learnline !== undefined) (function ($) {

learnline.Class('Edutags', {
  raw: null,
  tags: null,
  logo: null,
  rating: null,
  toggle: null,
  tagBox: null,
  ratingBox: null,
//    toggleList: null,
  $container: null,

  /**
   * Class initialization/costructor.
   *
   * The 'edutags_result' parameter must at least contain a 'resource' key,
   * holding the resource's URL. Otherwise 'edmond_result' should be the
   * Edutags-API result for the given 'element' parameter.
   *
   * @param {Element} element
   * @param {Object} edutags_result
   * @returns {learnline.Edutags}
   */
  initialize: function (element, edutags_result) {
    this.domain = Drupal.settings.learnline.edutags.domain,
    this.bookmarklet_path = Drupal.settings.learnline.edutags.bookmarklet_path;

    this.raw = edutags_result,
    this.$container = element;

    this.tagBox = this.$container.find('.search-result-edutags-box'),
    this.ratingBox = this.$container.find('.search-result-rating');
//      this.toggleList = this.$container.find('.search-result-trigger');

    return edutags_result.html ? this.show() : this.fake();
  },

  /**
   * Render/place Edutags components for this element.
   *
   * @returns {learnline.Edutags}
   */
  show: function () {
    var self = this; // We'll use this as reference in callback functions.
    // Move elements to the right place...
    $(this.tagBox).html(this.raw.html);
    this.tags = $(this.tagBox).children('.edutags-tags'),
    this.logo = $(this.tagBox).children('i.fa.fa-fake-edutags').detach()
        .appendTo(this.ratingBox);

    this.rating = $(this.tagBox).children('.edutags-rating').detach()
        .appendTo(this.ratingBox)
        .hover(this.colorize, this.decolorize)
        .click(function (e) {
          e.preventDefault();
//          self.bookmarklet('', '', '', '', 'vote', self.raw.id);
          self.vote();
          return false;
        });

//      this.toggle = $(this.tagBox).children('.edutags-toggle').detach()
//          .prependTo(this.toggleList).wrap($('<li></li>'));

    this.toggle = $(this.tagBox).children('a.edutags-toggle').detach()
        .insertAfter(this.rating).prepend('&nbsp;')
        .hover(this.colorize, this.decolorize);

    // Initialize toggle slider.
    new learnline.ToggleSlider(this.toggle, this.tagBox, { state: false });

    // Assign tag callback.
    this.$container.find('.edutags-button-tag')
        .click(function (e) {
          e.preventDefault();
          var params = self.fetchInitialData();
          self.bookmarklet(
              params.nodeTags,
              params.nodeTitle,
              params.url,
              params.nodeDescription,
              'tag',
              0
          );

          return false;
        });

    // Assign comment callback.
    this.$container.find('.edutags-button-comment')
        .click(function (e) {
          e.preventDefault();
//          self.bookmarklet('', '', '', '', 'comment', self.raw.id);
          self.comment();
          return false;
        });

    return this;
  },

  /**
   * Render empty Edutags components.
   *
   * This will be used for every resource not yet recognized by Edutags.
   *
   * @returns {learnline.Edutags}
   */
  fake: function () {
    var self = this;

    // Fake tags.
    this.tags = $('#edutags-fake').children('.edutags-tags').clone().appendTo(this.tagBox);
    // Fake comments.
    this.comments = $('#edutags-fake').children('.edutags-comments').clone().appendTo(this.tagBox);
    // Fake voting.
    this.logo = $('#edutags-fake').children('i.fa.fa-fake-edutags').clone().appendTo(this.ratingBox);
    this.rating = $('#edutags-fake').children('.edutags-rating').clone().appendTo(this.ratingBox)
        .hover(this.colorize, this.decolorize)
        .click(function (e) {
          e.preventDefault();
//          self.bookmarklet('', '', '', '', 'vote', self.raw.id);
          self.vote();
          return false;
        });
    // Create toggle slider trigger.
    this.toggle = $('#edutags-fake').children('.edutags-toggle').clone()
        .hover(this.colorize, this.decolorize)
        .insertAfter(this.logo).prepend('&nbsp;');

    this.logo.hover(this.colorize, this.decolorize);

    // Initialize toggle slider.
    new learnline.ToggleSlider(this.toggle, this.tagBox, { state: false });
//    new learnline.ToggleSlider(this.rating, this.tagBox, { state: false });
//    new learnline.ToggleSlider(this.logo, this.tagBox, { state: false });

    // Assign tag callback.
    this.$container.find('.edutags-button-tag')
        .click(function (e) {
          e.preventDefault();
          var params = self.fetchInitialData();
          self.bookmarklet(
              params.nodeTags,
              params.nodeTitle,
              params.url,
              params.nodeDescription,
              'tag',
              0
          );
          return false;
        });

    // Assign comment callback.
    this.$container.find('.edutags-button-comment')
        .click(function (e) {
          e.preventDefault();
//          self.bookmarklet('', '', '', '', 'comment', self.raw.id);
          self.comment();
          return false;
        });
  },

  /**
   * Fetch Edutags relevant data from the element adn return it as
   * well-structured JS Object.
   *
   * This function is used by
   *   - learnline.Edutags.vote()
   *   - learnline.Edutags.comment()
   *   - learnline.Edutags.bookmarklet()
   *
   * @returns {Object}
   */
  fetchInitialData: function () {
    // Fetch initial tags from school subject.
    var tags = [],
        raw_subjects = $(this.$container.find('.search-result-subject[data-subject]')).data('subject'),
        raw_media_types = $(this.$container.find('span[data-media-type]')).data('media-type');

    if (raw_subjects || raw_media_types) {
      // Restore subject(s) as JSON array.
      var subjects = JSON.parse(atob(raw_subjects));
      var media_types = JSON.parse(atob(raw_media_types));
      // Iterate subjects
      for (var idx in $.merge(subjects, media_types)) {
        // Split subject off, if it contains any comma.
        $(subjects[idx].split(',')).each(function () {
          // Trim (splitted parts of the) subject and push them to tags.
          tags.push(String(this).trim());
        });
      }
    } //if

    // Fetch resource title.
    var title = $.trim(this.$container.find('h4[data-title]').text());

    // Fetch resource description.
    var description = $.trim(this.$container.find('.search-result-description').text());

    return {
      url: this.raw.resource,
      nodeTitle: title,
      nodeDescription: description,
      nodeTags: tags.join(',')
    };
  },

  /**
   * Vote for the element's resource on Edutags.
   *
   * Opens a new browser window with a comment and authentication fields.
   *
   * @returns {learnline.Edutags}
   */
  vote: function () {
    var params = [ 'portal=LL' ],
        values = this.fetchInitialData();

    for (var key in values) {
      params.push( key+'='+encodeURIComponent(values[key]) );
    }

    window.open(
        this.domain+'/export/bookmarks_export/edutags_voting.php?'+params.join('&'),
        'Bewerten',
        'toolbar=no,width=500,height=690'
    );

    return this;
  },

  /**
   * Write a comment for the element's resource on Edutags.
   *
   * Opens a new browser window with a comment and authentication fields.
   *
   * @returns {learnline.Edutags}
   */
  comment: function () {
    var params = [ 'portal=LL' ],
        values = this.fetchInitialData();

    for (var key in values) {
      params.push( key+'='+encodeURIComponent(values[key]) );
    }

    window.open(
        this.domain+'/export/bookmarks_export/edutags_comment.php?'+params.join('&'),
        'Kommentieren',
        'toolbar=no,width=500,height=690'
    );

    return this;
  },

  /**
   * Edutags' Bookmarklet function.
   *
   * @param {string} resTags A comma seperated list of tags
   * @param {string} resTitle The resource title
   * @param {string} resUrl The resource URL
   * @param {string} resDescription A brief description for the resource
   * @param {string} action Action to perform (default: 'tag')
   * @param {string} nid The Edutags Node-ID (default: 0)
   * @returns {learnline.Edutags}
   */
  bookmarklet: function (resTags, resTitle, resUrl, resDescription, action, nid) {
    var DrupalBookmarklet223355;

    (function (edutags) {
      var a = document.createElement('script');
      a.src = edutags.domain+edutags.bookmarklet_path+'?'+Math.floor( Math.random() * 1000000 );
      a.type = 'text/javascript';

      var b = function () {
        DrupalBookmarklet223355 = new DrupalBookmarklet(edutags.domain, 'sites/all/modules/bookmarklet', resTags, resTitle, resUrl, resDescription, action, nid);
      };

      a.onload = b;
      a.onreadystatechange = b;
      document.getElementsByTagName('head')[0].appendChild(a);
    })(this);

    return this;
  },

  /**
   * Callback for hover effect simulation on Edutags logo.
   *
   * @param {jQuery.Event} e
   * @returns {learnline.Edutags}
   */
  colorize: function (e) {
    $(this).parents('.search-result').find('i.fa-fake-edutags').addClass('colorized');
    return this;
  },

  /**
   * Callback for hover effect simulation on Edutags logo.
   *
   * @param {jQuery.Event} e
   * @returns {learnline.Edutags}
   */
  decolorize: function (e) {
    $(this).parents('.search-result').find('i.fa-fake-edutags').removeClass('colorized');
    return this;
  }
});

})(jQuery);
