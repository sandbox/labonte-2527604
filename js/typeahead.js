// $Id:$

/**
 * @file
 * Javascript implementation of type-ahead functionality.
 */

if (window.learnline !== undefined) (function ($) {

/**
 * class TypeAhead
 *
 * @param {string} inputElement CSS selector of the search input element.
 * @returns {TypeAhead}
 */
var TypeAhead = learnline.Class('TypeAhead', {
  /**
   * A set of default options.
   */
  stdOpts: {
    uri: '/sodis/type-ahead/',
    uriCallback: function (input) { return this.uri + input; },
    input: 'input[data-learnline-autocomplete="on"]',
    observer: 'propertychange keyup input paste',
    container: '#search-box',
    wrapper: '<div id="autocompletion-wrapper"></div>',
    list: '<ul></ul>'
  },

  /**
   * Constructor.
   *
   * @param   {Object} options  All options are optional! Possible options can
   *                            be determined by looking at 'this.stdOpts'.
   */
  initialize: function (options) {
    this.request = this.response = 0;
    this.options = typeof options === 'Object' ? options : this.stdOpts;
    for (var opt in this.stdOpts) if (this.options[opt] === undefined) {
      this.options[opt] = this.stdOpts[opt];
    }

    this.input = $(this.options.input);
    this.input.data('last', this.input.val());
    this.wrapper = $(this.options.wrapper).appendTo($('body')).hide();

    var self = this;
    this.input.on(this.options.observer, function () {
      // Return immediately if nothing has changed.
      if (self.input.data('last') === self.input.val()) {
        return;
      }
      // Store current value and then return, if the value has less than three 
      // characters.
      if ($.trim(self.input.val()).length < 3) {
        self.input.data('last', self.input.val());
        return;
      }

      self.input.data('last', self.input.val());
      // Start AJAX request.
      var count = self.ajaxInit();
      $.getJSON(self.options.uriCallback(self.input.val()))
          .done(function (data) {
            if (count >= self.response) {
              self.response = count;
            } //if
            else {
              return;
            } //else

            var list = $(self.options.list);

            $.each(data, function (idx) {
              list.append($('<li></li>')
                  .html(this.toString())
                  .on('click.typeAhead.item' + idx, function () {
                    self.input.val($(this).text());
                  })
                  /**
                   * This would be the standard "hover" behavior for the list
                   * items.
                   */
//                    .hover(
//                      function () {
//                          $(this).parent().children('.selected').removeClass('selected');
//                          $(this).addClass('selected'); },
//                      function () { $(this).removeClass('selected'); }
//                    )
                  /**
                   * This is an alternative "hover" effect with persistent
                   * behavior considering the fact, that the hover effect has
                   * to co-exist with the arrow key selection and that the
                   * list and thereby the selection will be closed anyway.
                   */
                  .mouseover(function () {
                    $(this).parent().children('.selected').removeClass('selected');
                    $(this).addClass('selected');
                  })
              );
            });

            self.wrapper.show()
                .css(self.input.offset())
                .css('width', self.input.width())
                .html(list);

            self.item_first = self.wrapper.find('li:first-of-type');
            self.item_last = self.wrapper.find('li:last-of-type');

            self.used_arrow_keys = false;

            self.input.off('keydown.typeAhead');
            self.input.on('keydown.typeAhead', function (e) {
              if (!self.wrapper.is(':visible')) {
                return;
              } //if

              var selected = self.wrapper.find('li.selected');

              var next;
              switch (e.which) {
                case 38: //Arrow: UP
                  if (selected.length < 1) {
                    selected = self.item_last;
                  } //if
                  self.used_arrow_keys = true;
//                    selected.removeClass('selected');
                  self.wrapper.find('li.selected').removeClass('selected');
                  if ((next = selected.prev()).length < 1) {
                    next = self.item_last;
                  } //if
                  next.addClass('selected');
                  e.preventDefault();
                  break;
                case 40: //Arrow: DOWN
                  if (selected.length < 1) {
                    selected = self.item_last;
                  } //if
                  self.used_arrow_keys = true;
//                    selected.removeClass('selected');
                  self.wrapper.find('li.selected').removeClass('selected');
                  if ((next = selected.next()).length < 1) {
                    next = self.item_first;
                  }
                  next.addClass('selected');
                  e.preventDefault();
                  break;
                case 13: //ENTER
                  if (selected.length === 1) {
                    self.input.val(selected.text());
                    $(document).off('.typeAhead');
                    self.wrapper.hide();
                    if (self.used_arrow_keys) {
                      e.preventDefault();
                    } //if
                  } //if
                  break;
              } //switch/case
            });

            $(document).on('click.typeAhead', function () {
              $(document).off('.typeAhead');
              self.wrapper.hide();
            });
          })
          .fail(function () {})
          .always(self.ajaxComplete);
    });
  },

  ajaxInit: function () {
    this.DEBUG&2 && console.log('Loading image should now show up.');
    return this.request++;
  },

  ajaxComplete: function () {
    this.DEBUG&2 && console.log('Loading image should now disappear.');
  }
});

})(jQuery);
