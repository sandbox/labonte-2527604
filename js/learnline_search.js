// $Id:$

/**
 * @file
 * Module's Javascript snippets.
 *
 * @todo Consider renaming function uebersicht().
 */

/**
 * This code depends on the window.learnline object, provided by the learnline
 * theme!
 */

if (window.learnline !== undefined) (function ($) {

/**
 * Execute after the DOM has completed loading.
 */
Drupal.behaviors.learnline_search = {
  attach: function (context, settings) {
    /**
     * Additional event handler toggling the font-awesome arrow class.
     *
     * @param {learnline.ToggleSlider} self
     */
    var toggleFAArrow = function (self) {
      self.trigger.find('i.fa')
          .removeClass(self.visible ? 'fa-arrow-up' : 'fa-arrow-down')
          .addClass(self.visible ? 'fa-arrow-down': 'fa-arrow-up');
    }; //function

    /**
     * Additional event handler toggling the words 'more' and 'less' on each
     * slider action.
     *
     * @param {learnline.ToggleSlider} self
     */
    var toggleMoreText = function (self) {
      var moreText = {
        more: 'less',
        less: 'more',
        mehr: 'weniger',
        weniger: 'mehr'
      };

      for (var current in moreText) if (self.trigger.html().search(current) >= 0) {
        self.trigger.html(self.trigger.html().replace(current, moreText[current]));
        break;
      }
    }; //function

    /**
     * Conditionally initialize new learnline.ToggleSlider for the advanced
     * search options.
     */
    if ($('#advanced-search-toggle').length > 0) {
      new learnline.ToggleSlider('#advanced-search-toggle', '#advanced-search-container', {
        state: false,
        callback: [
          function (self) {
            if (self.visible) {
              self.trigger.find('i.fa')
                  .removeClass('fa-arrow-circle-up')
                  .addClass('fa-arrow-circle-down');
            }
            else {
              self.trigger.find('i.fa')
                  .removeClass('fa-arrow-circle-down')
                  .addClass('fa-arrow-circle-up');
            }
          },
          learnline.ToggleSlider.prototype.stdCallback
        ]
      });
    }

    /**
     * Add learnline.ToggleSlider for 'more' buttons on search results.
     */
    // First determine the slider state by looking for an approrpiate GET parameter.
    var exposed = location.search.search('exposed=1') > 0;
    $('.search-result-more-toggle').each(function () {
      var target = $(this).parent().next('div.search-result-more');
      new learnline.ToggleSlider(this, target, {
        state: exposed,
        callback: [
          toggleFAArrow,
          toggleMoreText,
          learnline.ToggleSlider.prototype.stdCallback
        ]
      });
    });

    /**
     * Add learnline.ToggleSlider for 'more' buttons on facets with more than five
     * values.
     */
    $('.search-result-facets .toggle-more').each(function () {
      new learnline.ToggleSlider(this, $(this).prev(), {
        state: false,
        callback: [
          toggleFAArrow,
          toggleMoreText,
          learnline.ToggleSlider.prototype.stdCallback
        ]
      });
    });

    /**
     * Add learnline.ToggleSlider for the search-result-trigger list.
     */
    $('ul.search-result-trigger > li').each(function () {
      var selector = 'div.' + $(this).attr('class') + '-box';
      var box = $(this).parents('div.search-result').find(selector);
      var callbacks = [ learnline.ToggleSlider.prototype.stdCallback ];

      switch ($(this).attr('class')) {
        case 'search-result-direct-link':
          callbacks.push(function (self) {
            self.target.find('input').select();
          });
          break;
      } //switch/case

      new learnline.ToggleSlider(this, box, {
        state: false,
        callback: callbacks
      });
    });

    /**
     * Apply preselection for advanced search options.
     */
    $(decodeURI(location.search).match(/[^&?=]+=[^&?=]+/g)).each(function () {
      var preselection = this.match(/([^&?=]+)=([^&?=]+)/);
      if (preselection.length > 2) {
        preselection[2] = preselection[2].replace(/([ !"#$%&'()*+,./:;<=>?@[\]^`{|}~\\])/g, '');
      }

      var selectables = [
        '#advanced-search-container input[type="checkbox"][name="' + preselection[1] + '"][value="' + preselection[2] + '"]',
        '#advanced-search-container input[type="radio"][name="' + preselection[1] + '"][value="' + preselection[2] + '"]',
        '#advanced-search-container select[name="' + preselection[1] + '"] option[value="' + preselection[2] + '"]'
      ];

      $(selectables).each(function () {
        $(''+this).prop('selected', true);
      });
      
      // Process text inputs.
      $('#advanced-search-container input[type*="text"][name="' + preselection[1] + '"]').val(preselection[2]);
    });

    /**
     * At least we check if there is any preselection for the EDMOND parameter.
     * If not, we'll pre-select the value 'Alle'.
     */
    if ($('#advanced-search-container select[name="edmond[]"]').val() === null) {
      $('#advanced-search-container select[name="edmond[]"]').val('');
    }
    
    /**
     * Initialize TypeAhead class if SODIS search is activated.
     */
    if (settings.learnline.typeahead.on) {
      new learnline.TypeAhead();
    }
    
    /**
     * Request and display Edutags data.
     */
    if (settings.learnline.edutags.on && location.pathname.match(/search$/)) {
      // Read IDs/URLs from resources data-url attribute
      var ids = [];
      var edutags = [];
      $('.search-result[data-url]').each(function () {
        var id = $(this).attr('data-url');
//        if ($.inArray(id, ids) < 0) {
//          ids.push(id);
//        }
        ids.push(id);
        edutags.push({
          element: $(this),
          url: $(this).attr('data-url')
        });
      });

      // Request Edutags data and render view
      $.ajax({
        url: '/edutags?urls=' + encodeURIComponent(JSON.stringify(ids)),
        beforeSend: function () {
          // Show loading indicator
          $('.edutags-placeholder').show();
        },
        success: function (data, status) {
          // Build mapper for edutags elements and edutags results
          var mapper = {};
          for (var i in edutags) {
            if (mapper[edutags[i].url] === undefined) 
              mapper[edutags[i].url] = [];
            mapper[edutags[i].url].push(i);
          }
          // Amend edutags array
          $(data).each(function () {
            for (var i in mapper[this.resource]) {
              edutags[mapper[this.resource][i]].response = this;
            }
          });
        },
        complete: function () {
          // Hide loading indicator
          $('.edutags-placeholder').hide();
          // Initialize Edutags
          $(edutags).each(function () {
            new learnline.Edutags(
                this.element, 
                this.response || { resource: this.url }
            );
          });
        }
      });
    }
    
    /**
     * Scroll to top.
     * This may seem superfluous, but some browsers will leave the user in the
     * middle of a page, if toggle text is initialized visible and hid when the
     * DOM stops loading.
     */
    window.scroll(0, 0);
  }
};

})(jQuery);
